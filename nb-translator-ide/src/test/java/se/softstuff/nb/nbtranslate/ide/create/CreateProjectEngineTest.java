package se.softstuff.nb.nbtranslate.ide.create;

import se.softstuff.nb.nbtranslate.ide.create.CreateProjectEngine;
import java.util.List;
import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.openide.util.editable.BrandedLocale;
import se.softstuff.nb.nbtranslate.data.NbTranslation;

/**
 *
 * @author Stefan
 */
public class CreateProjectEngineTest {
    
    private CreateProjectEngine instance;
    
    @Before
    public void setUp() {
        instance = new CreateProjectEngine();
    }

    
    @Test
    public void testCreateModuleIdentifyer() {
        String module = instance.createModuleIdentifyer("se.databyran.alf_branding_nbm_1.0-SNAPSHOT");
        assertEquals("se-databyran-alf-branding", module); 
   }
    
    @Test
    public void testCreateModuleType() {
        String module = instance.createModuleType("se.databyran.alf_branding_nbm_1.0-SNAPSHOT");
        assertEquals("nbm", module); 
   }
    
    @Test
    public void testCreateModuleVersion() {
        String module = instance.createModuleVersion("se.databyran.alf_branding_nbm_1.0-SNAPSHOT");
        assertEquals("1.0-SNAPSHOT", module); 
   }
    
    @Test
    public void testCreateResourceIdentifyer(){
        String resourceRoot = "C:/work/AlfProject/alf/src/main/resources";
        String bundle = "C:/work/AlfProject/alf/src/main/resources/se/databyran/alf/Bundle_foo_sv_SE.properties";
        String actual = instance.createResourceIdentifyer(bundle, resourceRoot);
        String expected = "se.databyran.alf.Bundle_foo_sv_SE.properties";
        assertEquals(expected,actual);
        
        bundle = "C:\\work\\AlfProject\\alf\\src\\main\\resources\\se\\databyran\\alf\\Bundle_foo_sv_SE.properties";
        actual = instance.createResourceIdentifyer(bundle, resourceRoot);
        assertEquals(expected,actual);
    }
    
    @Test
    public void testExpectedLocale(){
        
        assertTrue("default accept all", instance.expectedLocale("Bundle"));
        assertTrue("default accept all", instance.expectedLocale("Bundle_foo_bar"));
        
        instance.getLocales().add(new BrandedLocale("foo", Locale.UK));
        
        assertFalse("not accepted", instance.expectedLocale("Bundle"));
        assertFalse("not accepted", instance.expectedLocale("Bundle_foo_bar"));
        
        instance.getLocales().clear();
        instance.getLocales().add(new BrandedLocale(null, new Locale("")));
        
        assertTrue("accepted", instance.expectedLocale("Bundle"));
    }
    
    @Test
    public void localesAreAddedToExportObject() {
        instance.getLocales().add(new BrandedLocale(new Locale("junit", "CA")));
        instance.getLocales().add(new BrandedLocale(new Locale("junit", "UK")));
        List<String> locales = instance.createLocaleStrings();
        assertEquals(2, locales.size());
        assertEquals("junit_CA", locales.get(0));
        assertEquals("junit_UK", locales.get(1));
    }
    
    @Test
    public void defaultLocaleCanBeAddedToExportObject() {
        instance.getLocales().add(new BrandedLocale());
        List<String> locales = instance.createLocaleStrings();
        assertEquals(1, locales.size());
        assertEquals(NbTranslation.DEFAULT_LOCALE, locales.get(0));
    }
     
}