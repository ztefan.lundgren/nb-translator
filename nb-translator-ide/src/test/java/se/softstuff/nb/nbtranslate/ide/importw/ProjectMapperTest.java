package se.softstuff.nb.nbtranslate.ide.importw;

import se.softstuff.nb.nbtranslate.ide.importw.ProjectMapper;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author stefan
 */
public class ProjectMapperTest {
    
    public ProjectMapperTest() {
    }
    
    @Before
    public void setUp() {
    }

    @Test
    public void getModuleRef() {
        
        String actual = ProjectMapper.getModuleRef("se.mutter_mutter_nbm_1.2.3-SNAPSHOT");
        assertEquals("se-mutter-mutter", actual);
    }
}
