/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.softstuff.nb.nbtranslate.ide.importw;

/**
 *
 * @author Stefan
 */
public class Selection {
    
    public enum Error {
        NO_ERROR, MAP_PROJECT, ORIGIN_MISSMATCH, PROJECT_VERSION_MISSMATCH
    }

    public enum Status {

        OK, MANUAL, FAIL
    }

    public enum Choice {

        OK, IGNORE
    }
    
    private Status status;
    private Choice choice;
    private String message;
    private Error error;
    private String valueFromFile;

    public Selection(){
        this.status = Status.OK;
        this.choice = Choice.OK;
        this.message = "";
        this.error = Error.NO_ERROR;
    }
    public Selection(Status status, Choice choice, Error error, String message) {
        this.status = status;
        this.choice = choice;
        this.message = message;
        this.error = error;
    }

    public Status getStatus() {
        return status;
    }

    public Choice getChoice() {
        return choice;
    }

    public String getMessage() {
        return message;
    }

    public Selection setStatus(Status status) {
        this.status = status;
        return this;
    }

    public Selection setChoice(Choice choice) {
        this.choice = choice;
        return this;
    }

    public Selection setMessage(String message) {
        this.message = message;
        return this;
    }

    public Error getError() {
        return error;
    }

    public Selection setError(Error error) {
        this.error = error;
        return this;
    }

    public String getValueFromFile() {
        return valueFromFile;
    }

    public void setValueFromFile(String valueFromFile) {
        this.valueFromFile = valueFromFile;
    }
    
    
}
