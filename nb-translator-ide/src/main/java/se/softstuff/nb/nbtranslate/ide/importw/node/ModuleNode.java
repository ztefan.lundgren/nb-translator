/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.softstuff.nb.nbtranslate.ide.importw.node;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFileChooser;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;
import org.netbeans.api.project.ProjectManager;
import org.netbeans.api.project.ProjectUtils;
import org.openide.ErrorManager;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import se.softstuff.nb.nbtranslate.ide.create.CreateProjectVisualPanel1;
import se.softstuff.nb.nbtranslate.ide.importw.ImportProjectEngine;
import se.softstuff.nb.nbtranslate.ide.importw.Selection;
import se.softstuff.nb.nbtranslate.data.NbTranslation;

/**
 *
 * @author Stefan
 */
public class ModuleNode extends AbstractNode implements RefreshableNode {

    private static Image okFolder;

    private static Image failedFolder;

    private static Image manualFolder;

    private Module module;

    private ImportProjectEngine engine;

    private String lastLocation;

    static {
        okFolder = ImageUtilities.loadImage("se/softstuff/nb/nbtranslate/ide/importw/node/folder.png");
        manualFolder = ImageUtilities.loadImage("se/softstuff/nb/nbtranslate/ide/importw/node/folder_error.png");
        failedFolder = ImageUtilities.loadImage("se/softstuff/nb/nbtranslate/ide/importw/node/folder_delete.png");
    }

    public ModuleNode(Module module, NbTranslation translation, ImportProjectEngine engine) {
        super(Children.create(new ResourceChildFactory(module, translation, engine), false));
        this.module = module;
        this.engine = engine;
        setName(module.getModule());
        setDisplayName(module.getModule());
        setShortDescription(getToolTip());

    }

    @Override
    public Action[] getActions(boolean context) {
        List<Action> actions = new ArrayList<Action>(Arrays.asList(super.getActions(context)));

        switch (module.getSelection().getError()) {
            case MAP_PROJECT:
                actions.add(createMapProjectAction());
        }
        return actions.toArray(new Action[0]);
    }

    @Override
    public Image getIcon(int type) {
        return getFolderIcon();
    }

    @Override
    public Image getOpenedIcon(int type) {
        return getFolderIcon();
    }

    private Image getFolderIcon() {
        switch (module.getSelection().getStatus()) {
            case FAIL:
                return failedFolder;
            case MANUAL:
                return manualFolder;
            default:
                return okFolder;
        }
    }

    @Override
    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();

        try {
            Selection selection = module.getSelection();
            Property choiseProp = new PropertySupport.Reflection(selection, Selection.Choice.class, "getChoice", "setChoice");
//            Property statusProp = new PropertySupport.Reflection(selection, Selection.Status.class, "getStatus", null);
            Property messageProp = new PropertySupport.Reflection(selection, String.class, "getMessage", null);

            choiseProp.setName("choice");
//            statusProp.setName("status");
            messageProp.setName("message");

            set.put(choiseProp);
//            set.put(statusProp);
            set.put(messageProp);

        } catch (NoSuchMethodException ex) {
            ErrorManager.getDefault();
        }

        sheet.put(set);
        return sheet;
    }

    private Action createMapProjectAction() {
        return new AbstractAction() {
            {
                super.putValue(AbstractAction.NAME, NbBundle.getMessage(ModuleNode.class, "ModuleNode.createMapProjectAction.dispalyname"));
                super.putValue(AbstractAction.SHORT_DESCRIPTION, NbBundle.getMessage(getClass(), "ModuleNode.openProjectDialog.title", module.getModule()));
            }

            @Override
            public void actionPerformed(ActionEvent e) {

                popMapProjectsDialog();
            }
        };
    }

    private void popMapProjectsDialog() {

        JFileChooser projectChooser = new JFileChooser();
        if (lastLocation != null) {
            projectChooser.setCurrentDirectory(new File(lastLocation));
        }
        projectChooser.setFileFilter(new CreateProjectVisualPanel1.ProjectFileFilter());
        projectChooser.setFileView(new CreateProjectVisualPanel1.ProjectFileView());
        projectChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        projectChooser.setMultiSelectionEnabled(true);
        projectChooser.setDialogTitle(NbBundle.getMessage(getClass(), "ModuleNode.openProjectDialog.title", module.getModule()));
        projectChooser.showOpenDialog(null);
        final File projectToBeOpenedFile = projectChooser.getSelectedFile();
        if (projectToBeOpenedFile != null) {

            FileObject toFileObject = FileUtil.toFileObject(projectToBeOpenedFile);
            try {
                Project foundProject = ProjectManager.getDefault().findProject(toFileObject);
                ProjectInformation information = ProjectUtils.getInformation(foundProject);
                if (information != null) {
                    engine.addProject(information);

                    engine.doImport(false);
                    firePropertyChange(PROP_REFRESH, false, true);

                    lastLocation = projectToBeOpenedFile.getAbsolutePath();
                }


            } catch (IOException ioe) {
            }

            return;
        }
    }

    private String getToolTip() {
        switch (this.module.getSelection().getStatus()) {
            case FAIL:
                return NbBundle.getMessage(ValueRowNode.class, "Node.tooltip.error", module.getSelection().getMessage());
            case MANUAL:
                return NbBundle.getMessage(ValueRowNode.class, "Node.tooltip.manual", module.getSelection().getMessage());
            default:
                return NbBundle.getMessage(ValueRowNode.class, "Node.tooltip.ok");
        }
    }

    
    private boolean refresh;

    public boolean isRefresh() {
        return refresh;
    }

    public void setRefresh(boolean refresh) {
        this.refresh = refresh;
    }
    
}
