/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.softstuff.nb.nbtranslate.ide.importw.node;

import java.awt.Image;
import org.openide.ErrorManager;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import se.softstuff.nb.nbtranslate.ide.importw.ImportProjectEngine;
import se.softstuff.nb.nbtranslate.ide.importw.Selection;
import se.softstuff.nb.nbtranslate.data.NbTranslation;

/**
 *
 * @author Stefan
 */
class ResourceNode extends AbstractNode {

    private static Image okFolder;

    private static Image failedFolder;

    private static Image manualFolder;

    private Resource resource;

    static {
        okFolder = ImageUtilities.loadImage("se/softstuff/nb/nbtranslate/ide/importw/node/folder.png");
        manualFolder = ImageUtilities.loadImage("se/softstuff/nb/nbtranslate/ide/importw/node/folder_error.png");
        failedFolder = ImageUtilities.loadImage("se/softstuff/nb/nbtranslate/ide/importw/node/folder_delete.png");
    }

    public ResourceNode(Resource resource, NbTranslation translation, ImportProjectEngine engine) {
        super(Children.create(new ValueRowChildFactory(resource, translation, engine), false));
        this.resource = resource;
        setName(resource.getResource());
        setDisplayName(resource.getResource());
        setShortDescription(getToolTip());

    }

    @Override
    public Image getIcon(int type) {
        return getFolderIcon();
    }

    @Override
    public Image getOpenedIcon(int type) {
        return getFolderIcon();
    }

    private Image getFolderIcon() {
        switch (resource.getSelection().getStatus()) {
            case FAIL:
                return failedFolder;
            case MANUAL:
                return manualFolder;
            default:
                return okFolder;
        }
    }

    @Override
    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();

        try {
            Selection selection = resource.getSelection();
            Property choiseProp = new PropertySupport.Reflection(selection, Selection.Choice.class, "getChoice", "setChoice");
            Property messageProp = new PropertySupport.Reflection(selection, String.class, "getMessage", null);

            choiseProp.setName("choice");
            messageProp.setName("message");

            set.put(choiseProp);
            set.put(messageProp);

        } catch (NoSuchMethodException ex) {
            ErrorManager.getDefault();
        }

        sheet.put(set);
        return sheet;
    }

    private String getToolTip() {
        switch (this.resource.getStatus()) {
            case FAIL:
                return NbBundle.getMessage(ValueRowNode.class, "Node.tooltip.error", resource.getMessage());
            case MANUAL:
                return NbBundle.getMessage(ValueRowNode.class, "Node.tooltip.manual", resource.getMessage());
            default:
                return NbBundle.getMessage(ValueRowNode.class, "Node.tooltip.ok");
        }
    }
}
