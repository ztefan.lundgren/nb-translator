package se.softstuff.nb.nbtranslate.ide.importw;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import javax.swing.SwingWorker;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.DialogDisplayer;
import org.openide.WizardDescriptor;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

@ActionID(
    category = "Tools/NbTranslator",
id = "se.softstuff.nb.nbtranslate.ide.ImportWizardAction")
@ActionRegistration(
    displayName = "#CTL_ImportWizardAction")
@ActionReference(path = "Menu/Tools/NbTranslator", position = 100)
public final class ImportWizardAction implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        WizardDescriptor wiz = new WizardDescriptor(new ImportWizardIterator());
        // {0} will be replaced by WizardDescriptor.Panel.getComponent().getName()
        // {1} will be replaced by WizardDescriptor.Iterator.name()
        wiz.setTitleFormat(new MessageFormat("{0} ({1})"));
        wiz.setTitle("...dialog title...");
        if (DialogDisplayer.getDefault().notify(wiz) == WizardDescriptor.FINISH_OPTION) {
            ImportProjectEngine engine = (ImportProjectEngine)wiz.getProperty(ImportWizardDescriptor.IMPORT_ENGINE);
                
            new ImportWorker(engine).execute();
        }
    }

    private class ImportWorker extends SwingWorker<FileObject, Void> {

        private ImportProjectEngine importer;
        private ProgressHandle progress;

        public ImportWorker(ImportProjectEngine importer) {
            this.importer = importer;
        }

        @Override
        protected FileObject doInBackground() throws Exception {

            String progressMsg = NbBundle.getMessage(ImportWizardAction.class, "ImportWizardAction.progressMsg");
            progress = ProgressHandleFactory.createHandle(progressMsg);
            progress.start();
            
            FileSystem fs = FileUtil.createMemoryFileSystem();
            FileObject logFile = fs.getRoot().createData("import", "log");
            PrintWriter logger = new PrintWriter(logFile.getOutputStream());
            try {
                importer.setLogFile(logger);
                importer.doImport(true);

                logger.println();
                logger.println("Imported " + DateFormat.getDateTimeInstance().format(new Date()));


            } finally {
                logger.flush();
                logger.close();
            }

            return logFile;
        }

        @Override
        protected void done() {
            try {
                progress.finish();
                FileObject log = get();
                if (log != null) {
                    DataObject find = DataObject.find(log);
                    OpenCookie openCookie = find.getCookie(OpenCookie.class);
                    openCookie.open();
                }
            } catch (DataObjectNotFoundException ex) {
                Exceptions.printStackTrace(ex);
            } catch (InterruptedException ex) {
                Exceptions.printStackTrace(ex);
            } catch (ExecutionException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }
}
