package se.softstuff.nb.nbtranslate.ide.importw.node;

/**
 *
 * @author stefan
 */
public interface RefreshableNode {
    
    public static final String PROP_REFRESH = "refresh";
    
}
