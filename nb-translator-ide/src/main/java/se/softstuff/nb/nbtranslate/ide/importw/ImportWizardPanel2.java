package se.softstuff.nb.nbtranslate.ide.importw;

import java.util.Set;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;
import org.openide.util.HelpCtx;
import se.softstuff.nb.nbtranslate.data.NbTranslation;

public class ImportWizardPanel2 implements WizardDescriptor.Panel<WizardDescriptor> {

    /**
     * The visual component that displays this panel. If you need to access the
     * component from this class, just use getComponent().
     */
    private ImportChoicesPanel component;

    // Get the visual component for the panel. In this template, the component
    // is kept separate. This can be more efficient: if the wizard is created
    // but never displayed, or not all panels are displayed, it is better to
    // create only those which really need to be visible.
    @Override
    public ImportChoicesPanel getComponent() {
        if (component == null) {
            component = new ImportChoicesPanel();
        }
        return component;
    }

    @Override
    public HelpCtx getHelp() {
        // Show no Help button for this panel:
        return HelpCtx.DEFAULT_HELP;
        // If you have context help:
        // return new HelpCtx("help.key.here");
    }

    @Override
    public boolean isValid() {
        // If it is always OK to press Next or Finish, then:
        return true;
        // If it depends on some condition (form filled out...) and
        // this condition changes (last form field filled in...) then
        // use ChangeSupport to implement add/removeChangeListener below.
        // WizardDescriptor.ERROR/WARNING/INFORMATION_MESSAGE will also be useful.
    }

    @Override
    public void addChangeListener(ChangeListener l) {
    }

    @Override
    public void removeChangeListener(ChangeListener l) {
    }

    @Override
    public void readSettings(WizardDescriptor wiz) {
        NbTranslation translation = (NbTranslation)wiz.getProperty(ImportWizardDescriptor.TRANSLATION_PROJECT);
        Set<String> language = (Set<String>)wiz.getProperty(ImportWizardDescriptor.LOCALES);
        component.init(translation, language);
    }

    @Override
    public void storeSettings(WizardDescriptor wiz) {
        wiz.putProperty(ImportWizardDescriptor.IMPORT_ENGINE, component.getEngine());
    }
}
