package se.softstuff.nb.nbtranslate.ide.create;

import java.util.Collection;
import java.util.List;
import org.netbeans.api.project.Project;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

/**
 *
 * @author stefan
 */
public class SourceChildren extends Children.Keys<Project> {

    private Collection<? extends Project> projects;

    public SourceChildren(Collection<? extends Project> projects) {
        super(false);
        this.projects = projects;
    }
    
    @Override
    protected Node[] createNodes(Project project) {
        return new Node[] {new SourceProjectNode(project)};
    }

    @Override
    protected void addNotify() {
        setKeys(projects);
        super.addNotify();
    }

    @Override
    protected void removeNotify() {
        setKeys(projects);
        super.removeNotify();
    }
    
    
    
}
