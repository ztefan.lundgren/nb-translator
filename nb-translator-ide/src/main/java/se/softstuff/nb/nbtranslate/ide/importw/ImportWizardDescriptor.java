package se.softstuff.nb.nbtranslate.ide.importw;

/**
 *
 * @author stefan
 */
public class ImportWizardDescriptor {
    public static final String FILE = "file";
    public static final String TRANSLATION_PROJECT = "translationProject";
    public static final String SOURCE_PROJECTS = "sourceProject";
    public static final String LOCALES = "locales";
    public static final String IMPORT_ENGINE = "engine";
}
