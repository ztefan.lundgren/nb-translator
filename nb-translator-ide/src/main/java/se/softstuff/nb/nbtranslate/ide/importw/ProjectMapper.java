/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.softstuff.nb.nbtranslate.ide.importw;

import java.util.HashMap;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.api.project.ui.OpenProjects;

/**
 *
 * @author Stefan
 */
public class ProjectMapper {
    
    HashMap<String, Project> projects;
    public ProjectMapper(){
        projects = new HashMap<String, Project>();
    }
    
    public void fetchOpenProjects(){                
        for(Project open : OpenProjects.getDefault().getOpenProjects()){
            ProjectInformation info = ProjectUtils.getInformation(open);
            add(info);
        }
    }
    
    public void add(ProjectInformation info){
        String name = info.getName();
        name = getModuleRef(name);
        Project project = info.getProject();
        projects.put(name, project);
    }

    public Project get(String name) {
        return projects.get(name);
    }

    static String getModuleRef(String projectName) {
        String module = projectName.substring(0,projectName.lastIndexOf("_"));
        module = module.substring(0,module.lastIndexOf("_"));
        module = module.replace(".", "-").replace("_", "-");
        return module;
    }
}
