/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.softstuff.nb.nbtranslate.ide.importw.node;

import java.awt.Image;
import java.beans.IntrospectionException;
import org.openide.ErrorManager;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import se.softstuff.nb.nbtranslate.ide.importw.Selection;

/**
 *
 * @author Stefan
 */
class ValueRowNode extends AbstractNode {

    private ValueRow line;

    private static Image redDot;

    private static Image yellowDot;

    private static Image greenDot;

    static {
        redDot = ImageUtilities.loadImage("se/softstuff/nb/nbtranslate/ide/importw/node/red_dot.png");
        yellowDot = ImageUtilities.loadImage("se/softstuff/nb/nbtranslate/ide/importw/node/yellow_dot.png");
        greenDot = ImageUtilities.loadImage("se/softstuff/nb/nbtranslate/ide/importw/node/green_dot.png");
    }

    public ValueRowNode(ValueRow line) throws IntrospectionException {
        super(Children.LEAF);
        this.line = line;
        setName(line.getKey());
        setDisplayName(line.getKey());
        setShortDescription(getToolTip());
    }

    @Override
    public Image getIcon(int type) {
        return getIconType();
    }

    @Override
    public Image getOpenedIcon(int type) {
        return getIconType();
    }

    private Image getIconType() {
        switch (line.getStatus()) {
            case FAIL:
                return redDot;
            case MANUAL:
                return yellowDot;
            default:
                return greenDot;
        }
    }

    @Override
    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();

        try {
            Selection selection = line.getSelection();
            Property originProp = new PropertySupport.Reflection(selection, String.class, "getValueFromFile", null);
            Property exportedProp = new PropertySupport.Reflection(line, String.class, "getExported", null);
            Property changedProp = new PropertySupport.Reflection(line, String.class, "getChanged", null);

            Property choiseProp = new PropertySupport.Reflection(selection, Selection.Choice.class, "getChoice", "setChoice");
            Property messageProp = new PropertySupport.Reflection(selection, String.class, "getMessage", null);

            originProp.setName("origin");
            exportedProp.setName("exported");
            changedProp.setName("changed");
            choiseProp.setName("choice");
            messageProp.setName("message");

            set.put(originProp);
            set.put(exportedProp);
            set.put(changedProp);
            set.put(choiseProp);
            set.put(messageProp);

        } catch (NoSuchMethodException ex) {
            ErrorManager.getDefault();
        }

        sheet.put(set);
        return sheet;
    }

    private String getToolTip() {
        switch (line.getStatus()) {
            case FAIL:
                return NbBundle.getMessage(ValueRowNode.class, "Node.tooltip.error", line.getMessage());
            case MANUAL:
                return NbBundle.getMessage(ValueRowNode.class, "Node.tooltip.manual", line.getMessage());
            default:
                return NbBundle.getMessage(ValueRowNode.class, "Node.tooltip.ok");
        }
    }
}
