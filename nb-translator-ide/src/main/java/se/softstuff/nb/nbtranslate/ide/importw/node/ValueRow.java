/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.softstuff.nb.nbtranslate.ide.importw.node;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import se.softstuff.nb.nbtranslate.ide.importw.Selection;

/**
 *
 * @author Stefan
 */
public class ValueRow {
    
    public static final String PROP_CHANGED = "changed";
    
    private String module;
    private String resouce;
    private String key;
    private String exported;
    private Selection selection;

    private PropertyChangeSupport pcs;
    
    public ValueRow(String module, String resouce, String key, String exported, String changed, Selection selection) {
        this.module = module;
        this.resouce = resouce;
        this.key = key;
        this.exported = exported;
        this.changed = changed;
        this.selection = selection;
        if(selection==null){
            this.selection = new Selection(Selection.Status.FAIL, Selection.Choice.OK, Selection.Error.NO_ERROR, "");
        }
        
        pcs = new PropertyChangeSupport(this);
    }
        private String changed;


    public String getChanged() {
        return changed;
    }

    public void setChanged(String changed) {
        String oldChanged = this.changed;
        this.changed = changed;
        pcs.firePropertyChange(PROP_CHANGED, oldChanged, changed);
    }


    public String getModule() {
        return module;
    }

    public String getResouce() {
        return resouce;
    }

    public String getKey() {
        return key;
    }

    public String getExported() {
        return exported;
    }

    public Selection getSelection() {
        return selection;
    }
    
    public Selection.Status getStatus(){
        return selection.getStatus();
    }
    
    public Selection.Choice getChoice(){
        return selection.getChoice();
    }
    
    public void setChoice(Selection.Choice choice){
        selection.setChoice(choice);
    }
    
    public String getMessage(){
        return selection.getMessage();
    }
    
    
    public void addPropertyChangeListener(PropertyChangeListener listener){
        pcs.addPropertyChangeListener(listener);
    }
}
