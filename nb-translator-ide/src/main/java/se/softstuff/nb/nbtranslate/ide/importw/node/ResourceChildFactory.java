/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.softstuff.nb.nbtranslate.ide.importw.node;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import se.softstuff.nb.nbtranslate.ide.importw.ImportProjectEngine;
import se.softstuff.nb.nbtranslate.ide.importw.Selection;
import se.softstuff.nb.nbtranslate.data.NbTranslation;
import se.softstuff.nb.nbtranslate.marker.MarkerUtilities;

/**
 *
 * @author Stefan
 */
class ResourceChildFactory extends ChildFactory<Resource> {

    private Module module;

    private NbTranslation translation;
    
    private ImportProjectEngine engine;

    public ResourceChildFactory(Module module, NbTranslation translation, ImportProjectEngine engine) {
        this.module = module;
        this.translation = translation;
        this.engine = engine;
    }

    @Override
    protected boolean createKeys(List<Resource> toPopulate) {
        
        for (String resource : translation.getChangedResource(module.getModule()).keySet()) {
            String lang = MarkerUtilities.getBundleBrandingAndLocaleString(resource);

            if (!engine.validateLanguage(lang)) {
                continue;
            }
            Selection selection = engine.getSelectionsFor(String.format("%s:%s", module.getModule(), resource));
            toPopulate.add(new Resource(module.getModule(), resource, selection));
        }
        
        return true;
    }

    @Override
    protected Node createNodeForKey(Resource key) {
        Node node = new ResourceNode(key, translation, engine);
        return node;
    }
}
