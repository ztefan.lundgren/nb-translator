/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.softstuff.nb.nbtranslate.ide.importw.node;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import se.softstuff.nb.nbtranslate.ide.importw.ImportProjectEngine;
import se.softstuff.nb.nbtranslate.data.NbTranslation;

/**
 *
 * @author Stefan
 */
public class ModuleChildFactory  extends ChildFactory<Module> implements PropertyChangeListener {
    
    
    private NbTranslation translation;
    
    private ImportProjectEngine engine;
    
    public ModuleChildFactory( NbTranslation translation, ImportProjectEngine engine){
        this.translation = translation;
        this.engine = engine;
    }

    @Override
    protected boolean createKeys(List<Module> toPopulate) {
        for(String module : translation.getChangedModulesNames()){            
            toPopulate.add(new Module(module, engine.getSelectionsFor(module)));
        }
        return true;
        
    }

    @Override
    protected Node createNodeForKey(Module key) {
        ModuleNode node = new ModuleNode(key, translation, engine);
        node.addPropertyChangeListener(this);
        return node;
    }
    
    

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if(evt.getPropertyName().equals(RefreshableNode.PROP_REFRESH)){
            refresh(true);
        }
    }
    
}
