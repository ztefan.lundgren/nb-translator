/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.softstuff.nb.nbtranslate.ide.importw.node;

import se.softstuff.nb.nbtranslate.ide.importw.Selection;

/**
 *
 * @author Stefan
 */
public class Module {
    private String module;
    private Selection selection;

    public String getModule() {
        return module;
    }

    
    public Module(String module, Selection selection) {
        this.module = module;
        this.selection = selection;
        if(selection==null){
            this.selection = new Selection(Selection.Status.FAIL, Selection.Choice.OK,Selection.Error.NO_ERROR, "No changes");
        }
    }
    
    public Selection getSelection() {
        return selection;
    }
    
    
    
}
