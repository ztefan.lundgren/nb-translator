package se.softstuff.nb.nbtranslate.ide.importw;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;
import org.netbeans.api.java.project.JavaProjectConstants;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.api.project.SourceGroup;
import org.netbeans.api.project.Sources;
import org.netbeans.api.queries.FileEncodingQuery;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import se.softstuff.nb.nbtranslate.data.NbTranslation;
import se.softstuff.nb.nbtranslate.data.ProjectBundleInfo;
import se.softstuff.nb.nbtranslate.marker.MarkerUtilities;
import se.softstuff.nb.nbtranslate.util.EncodingUtil;

/**
 *
 * @author stefan
 */
public class ImportProjectEngine {

    private static final String KEY_VALUE_DELIMETER = "=";
    public static final String SEPARATOR = "_";
    public static final String NEW_LINE = "\n";

    private Logger log = Logger.getLogger(ImportProjectEngine.class.getName());

    private NbTranslation translation;

    private Set<String> language;

    private ProjectMapper projectMapper;

    private Map<String, Selection> selections;

    private boolean execute;
    
    private PrintWriter logger;
    
    private FileSystem fs;

    ImportProjectEngine() {
        this.selections = new HashMap<String, Selection>();
    }

    public void setExecute(boolean execute) {
        this.execute = execute;
    }

    public Map<String, Selection> getSelections() {
        return selections;
    }

    public void addProject(ProjectInformation project) {
        projectMapper.add(project);
    }

    void start(NbTranslation translation, Set<String> language, ProjectMapper projectMapper, boolean execute) {
        this.translation = translation;
        this.language = language;
        this.projectMapper = projectMapper;

        doImport(execute);
    }

    public void doImport(boolean execute) {
        log.info("Begin import translations");
        this.execute = execute;
        fs = FileUtil.createMemoryFileSystem();


        Set<String> modules = translation.getChangedModulesNames();
        for (String moduleRef : modules) {

            boolean valide = validateModlule(moduleRef);
            if (!valide) {
                continue;
            }
            log("Process module: "+moduleRef);
            ProjectBundleInfo knownInfo = translation.getKnownProjectMap().get(moduleRef);
            Project project = projectMapper.get(knownInfo.getModuleRef());


            Map<String, Properties> changedResource = translation.getChangedResource(moduleRef);

            for (String bundleRef : changedResource.keySet()) {
                String lang = MarkerUtilities.getBundleBrandingAndLocaleString(bundleRef);

                valide = validateLanguage(lang);
                if (!valide) {
                    log(String.format("Ignore bundle %s, not allowed language", bundleRef));
                    continue;
                }

                Properties original = getOriginal(moduleRef, bundleRef);
                Properties changes = changedResource.get(bundleRef);

                if (changes.isEmpty()) {
                    log(String.format("Ignore bundle %s, no changes was detected", bundleRef));
                    continue;
                }
                importDiff(moduleRef, bundleRef, changes, original, project);
            }

        }
    }

    private void importDiff(String module, String resource, Properties changes, Properties original, Project project) {

        
        FileObject bundleFile = findFile(resource, project);
        FileObject outFile = null;
        try {
            outFile = fs.getRoot().createData(resource, "");
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
        if (bundleFile == null) {
            log(String.format("Failed to find file for bundleRef:%s failed to update %s values", resource, changes.size()));
            return;
        }

        try {
            log("  Process bundle: "+resource);
            Set<String> changedKeys = new HashSet<String>(changes.stringPropertyNames());
            Charset encoding = FileEncodingQuery.getEncoding(bundleFile);
            List<String> lines = bundleFile.asLines(encoding.name());

            String resourceKey = String.format("%s:%s", module, resource);
            Selection selectionResource = getOrAddSelection(resourceKey, new Selection());
            if (selectionResource.getChoice() == Selection.Choice.IGNORE) {
                log(NbBundle.getMessage(ImportProjectEngine.class, "ImportProjectEngine.message.ignored", "  Resource", module));
                return;
            }
            
            PrintStream out = null;
            if (execute) {
                out = new PrintStream(outFile.getOutputStream(), false, encoding.name());
            }
            boolean wasUpdated = false;
            

            try {
                for (String line : lines) {
                    int eq = line.indexOf(KEY_VALUE_DELIMETER);
                    if (eq == -1) {
                        if (execute) {
                            out.println(line);
                        }
                        continue;
                    }
                    if (line.startsWith("#")) {
                        if (execute) {
                            out.println(line);
                        }
                        continue;
                    }
                    line = EncodingUtil.loadConvert(line);
                    String key = line.substring(0, eq);
                    String value = line.substring(eq + 1);
                    String expectedValue = original.getProperty(key);

                    String mapkey = String.format("%s:%s:%s", module, resource, key);
                    boolean isSelectionDefault = !selections.containsKey(mapkey);
                    Selection selection = getOrAddSelection(mapkey, new Selection());
                    selection.setValueFromFile(value);

                    if (expectedValue != null && !expectedValue.equals(value)) {
                        String missmatch = NbBundle.getMessage(ImportProjectEngine.class, "ImportProjectEngine.message.missmatch", value);
                        selection.setStatus(Selection.Status.MANUAL).setMessage(missmatch).setError(Selection.Error.ORIGIN_MISSMATCH);
                        selectionResource.setStatus(Selection.Status.MANUAL).setMessage(missmatch);
                        if (isSelectionDefault) {
                            selection.setChoice(Selection.Choice.IGNORE);
                            selectionResource.setChoice(Selection.Choice.IGNORE);
                        }
                    }

                    // origin values is equals
                    if (selection.getChoice() == Selection.Choice.OK && changes.containsKey(key)) {
                        value = changes.getProperty(key);
                        String message = NbBundle.getMessage(ImportProjectEngine.class, "ImportProjectEngine.message.modified");
                        selection.setMessage(message);
                        log(String.format("    %s=%s %s",key, value, message));
                        wasUpdated = true;
                    }

                    if (selection.getChoice() == Selection.Choice.IGNORE){
                        log(NbBundle.getMessage(ImportProjectEngine.class, "ImportProjectEngine.message.ignored", "    Row", String.format("%s=%s",key, value)));
                    }

                    // remove key to find added rows
                    changedKeys.remove(key);


                    if (execute) {
                        out.print(key);
                        out.print(KEY_VALUE_DELIMETER);
                        out.print(value);
                        out.print(NEW_LINE);
                    }
                }

                // add new rows
                for (String key : changedKeys) {
                    String value = changes.getProperty(key);
                    String mapkey = String.format("%s:%s:%s", module, resource, key);
                    String added = NbBundle.getMessage(ImportProjectEngine.class, "ImportProjectEngine.message.added" );
                    Selection selection = getOrAddSelection(mapkey, new Selection().setMessage(added));

                    if (execute && selection.getStatus() == Selection.Status.OK) {
                        out.print(key);
                        out.print(KEY_VALUE_DELIMETER);
                        out.print(value);
                        out.print(NEW_LINE);
                        log(String.format("    %s=%s %s",key, value, added));
                        wasUpdated = true;
                    }
                }
            } finally {
                if (execute) {
                    out.flush();
                    out.close();
                }
            }
            
            if (execute && wasUpdated) {
                bundleFile.delete();
                FileUtil.moveFile(outFile, bundleFile.getParent(), bundleFile.getName());
            }
            
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
            log(ex.getMessage());
        } 
    }

    private Properties getOriginal(String moduleRef, String bundleRef) {
        Map<String, Properties> originalResource = translation.getOriginalResource(moduleRef);
        if (originalResource != null) {
            Properties original = originalResource.get(bundleRef);
            return original;
        }
        return new Properties();
    }

    private FileObject findFile(String bundleRef, Project project) {

        String relativeFilePath = convertRefToFilePath(bundleRef);

        Sources sources = ProjectUtils.getSources(project);

        SourceGroup[] sourceGroups = sources.getSourceGroups(JavaProjectConstants.SOURCES_TYPE_RESOURCES);

        for (SourceGroup resources : sourceGroups) {
            FileObject fileObject = resources.getRootFolder().getFileObject(relativeFilePath);
            if (fileObject != null) {
                return fileObject;
            }
        }
        return null;
    }

    static String convertRefToFilePath(String bundleRef) {

        int lastDot = bundleRef.lastIndexOf(".");
        String replace = bundleRef.replace(".", "/");
        StringBuilder path = new StringBuilder(replace);
        path.setCharAt(lastDot, '.');
        return path.toString();
    }

    public Selection getSelectionsFor(String key) {
        return selections.get(key);
    }

    private Selection getOrAddSelection(String key, Selection newSelection) {
        Selection selection = selections.get(key);
        if (selection == null) {
            selection = newSelection;
            selections.put(key, newSelection);
        }
        return selection;
    }

    private Selection putSelection(String key, Selection newSelection) {
        selections.put(key, newSelection);
        return newSelection;
    }

    public boolean validateModlule(String module) {
        ProjectBundleInfo knownInfo = translation.getKnownProjectMap().get(module);
        Project project = projectMapper.get(knownInfo.getModuleRef());

        
        Selection selection = putSelection(module, new Selection());
        
        if(selection.getChoice() == Selection.Choice.IGNORE){
            log(NbBundle.getMessage(ImportProjectEngine.class, "ImportProjectEngine.message.ignored", "Module", module));
            return false;
        }
        
        if (project == null) {
            String msg = NbBundle.getMessage(ImportProjectEngine.class, "ImportProjectEngine.message.openproject" );
            selection.setStatus(Selection.Status.FAIL).setChoice(Selection.Choice.IGNORE).setError(Selection.Error.MAP_PROJECT).setMessage(msg);
            log(msg);
            return false;
        }
        ProjectInformation info = ProjectUtils.getInformation(project);
        String name = info.getName();
        if (!knownInfo.getName().equals(name)) {
            String expectedVersion = knownInfo.getVersion();
            String actualVersion = extractVersion(info.getName());
            selection.setStatus(Selection.Status.MANUAL).setError(Selection.Error.PROJECT_VERSION_MISSMATCH)
                    .setMessage(NbBundle.getMessage(ImportProjectEngine.class, "ImportProjectEngine.message.projectversion" , actualVersion, expectedVersion));
        }

        return true;
    }

    public boolean validateLanguage(String lang) {
        if (lang == null || !language.contains(lang)) {
            return false;
        }
        return true;
    }

    private String extractVersion(String projectName) {

        int begin = projectName.lastIndexOf(SEPARATOR);
        String version = projectName.substring(begin + 1);
        return version;
    }

    
    private void log(String message) {
        log.fine(message);
        if(logger != null){
            logger.println(message);
        }
    }

    
    void setLogFile(PrintWriter logger) {
        this.logger = logger;
    }
}
