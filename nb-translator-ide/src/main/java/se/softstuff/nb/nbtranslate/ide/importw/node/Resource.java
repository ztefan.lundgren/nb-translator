/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.softstuff.nb.nbtranslate.ide.importw.node;

import se.softstuff.nb.nbtranslate.ide.importw.Selection;

/**
 *
 * @author Stefan
 */
class Resource {

    private String module;
    private String resource;
    private Selection selection;

    public Resource(String module, String resource, Selection selection) {
        this.module = module;
        this.resource = resource;
        this.selection = selection;
        if (selection == null) {
            this.selection = new Selection(Selection.Status.FAIL, Selection.Choice.OK, Selection.Error.NO_ERROR,"");
        }
    }

    public String getModule() {
        return module;
    }

    public String getResource() {
        return resource;
    }

    public Selection getSelection() {
        return selection;
    }
    
    

    public Selection.Status getStatus() {
        return selection.getStatus();
    }

    public Selection.Choice getChoice() {
        return selection.getChoice();
    }

    public void setChoice(Selection.Choice choice) {
        selection.setChoice(choice);
    }

    public String getMessage() {
        return selection.getMessage();
    }
}
