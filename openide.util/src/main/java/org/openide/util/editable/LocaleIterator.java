package org.openide.util.editable;

import java.util.Iterator;
import java.util.Locale;
import java.util.NoSuchElementException;

/**
 * This class (enumeration) gives all localized sufixes using nextElement
 * method. It goes through given Locale and continues through
 * Locale.getDefault() Example 1: Locale.getDefault().toString() -> "_en_US" you
 * call new LocaleIterator(new Locale("cs", "CZ")); ==> You will gets: "_cs_CZ",
 * "_cs", "", "_en_US", "_en"
 *
 * Example 2: Locale.getDefault().toString() -> "_cs_CZ" you call new
 * LocaleIterator(new Locale("cs", "CZ")); ==> You will gets: "_cs_CZ", "_cs",
 * ""
 *
 * If there is a branding token in effect, you will get it too as an extra
 * prefix, taking precedence, e.g. for the token "f4jce":
 *
 * "_f4jce_cs_CZ", "_f4jce_cs", "_f4jce", "_f4jce_en_US", "_f4jce_en", "_cs_CZ",
 * "_cs", "", "_en_US", "_en"
 *
 * Branding tokens with underscores are broken apart naturally: so e.g. branding
 * "f4j_ce" looks first for "f4j_ce" branding, then "f4j" branding, then none.
 */
public class LocaleIterator extends Object implements Iterator<String>, Iterable<String> {

    @Override
    public Iterator<String> iterator() {
        return this;
    }

    public static class Item {
        public final String result;
        public final Locale locale;

        public Item(String result, Locale locale) {
            this.result = result;
            this.locale = locale;
        }
        
    }
    /**
     * this flag means, if default locale is in progress
     */
    private boolean defaultInProgress = false;
    /**
     * this flag means, if empty suffix was exported yet
     */
    private boolean empty = false;
    /**
     * current locale, and initial locale
     */
    private Locale locale;
    /**
     * current locale, and initial locale
     */
    private Locale initLocale;
    /**
     * current suffix which will be returned in next calling nextElement
     */
    private String current;
    /**
     * the branding string in use
     */
    private String branding;

    /**
     * Creates new LocaleIterator for given locale.
     *
     * @param locale given Locale
     */
    public LocaleIterator(String brandingToken, Locale locale) {
        this.locale = this.initLocale = locale;

        if (locale.equals(Locale.getDefault())) {
            defaultInProgress = true;
        }

        current = '_' + locale.toString();

        if (brandingToken == null) {
            branding = null;
        } else {
            branding = "_" + brandingToken; // NOI18N
        }

        //System.err.println("Constructed: " + this);
    }

    /**
     * @return next suffix.
     * @exception NoSuchElementException if there is no more locale suffix.
     */
    public @Override
    String next() throws NoSuchElementException {
        if (current == null) {
            throw new NoSuchElementException();
        }

        final String ret;

        if (branding == null) {
            ret = current;
        } else {
            ret = branding + current;
        }

        int lastUnderbar = current.lastIndexOf('_');

        if (lastUnderbar == 0) {
            if (empty) {
                reset();
            } else {
                current = ""; // NOI18N
                empty = true;
            }
        } else {
            if (lastUnderbar == -1) {
                if (defaultInProgress) {
                    reset();
                } else {
                    // [PENDING] stuff with trying the default locale
                    // after the real one does not actually seem to work...
                    locale = Locale.getDefault();
                    current = '_' + locale.toString();
                    defaultInProgress = true;
                }
            } else {
                current = current.substring(0, lastUnderbar);
            }
        }

        //System.err.println("Returning: `" + ret + "' from: " + this);
        return ret;
    }

    /**
     * Finish a series. If there was a branding prefix, restart without that
     * prefix (or with a shorter prefix); else finish.
     */
    private void reset() {
        if (branding != null) {
            current = '_' + initLocale.toString();

            int idx = branding.lastIndexOf('_');

            if (idx == 0) {
                branding = null;
            } else {
                branding = branding.substring(0, idx);
            }

            empty = false;
        } else {
            current = null;
        }
    }

    /**
     * Tests if there is any sufix.
     */
    public @Override
    boolean hasNext() {
        return (current != null);
    }

    public @Override
    void remove() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }
}
