package org.openide.util.editable;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 *
 * @author Stefan
 */
public interface NbResourceBundler {
    
    String getBranding();
    void setBranding(String bt) throws IllegalArgumentException;
    
    ResourceBundle getBundle(String baseName, Locale locale, ClassLoader loader) throws MissingResourceException;
    
}
