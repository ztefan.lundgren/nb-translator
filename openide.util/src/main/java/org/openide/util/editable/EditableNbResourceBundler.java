package org.openide.util.editable;

import java.util.Locale;
import java.util.MissingResourceException;

/**
 *
 * @author Stefan
 */
public interface EditableNbResourceBundler extends NbResourceBundler {
    
    @Override
    public EditableResourceBundle getBundle(String baseName, Locale locale, ClassLoader loader) throws MissingResourceException;    
        
    public EditableResourceBundle getBundle(String branding, String baseName, Locale locale, ClassLoader loader);

    public EditableResourceBundle getBundle(String branding, String baseName, Locale locale, ClassLoader loader, boolean useFallback);
}
