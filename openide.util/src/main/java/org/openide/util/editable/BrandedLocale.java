package org.openide.util.editable;

import java.util.Locale;
import java.util.MissingResourceException;
import org.openide.util.NbBundle;

/**
 *
 * @author Stefan
 */
public class BrandedLocale {
    
    public static final Locale DEFAULT_FALLBACK = new Locale("");
    
    
    private String branding;
    private Locale locale;

    public BrandedLocale() {
       this(null, DEFAULT_FALLBACK);
    }
    
    public BrandedLocale(String branding) {
        this(branding, DEFAULT_FALLBACK);
    }
    
    public BrandedLocale(Locale locale) {
       this(null, locale);
    }

    public BrandedLocale(String branding, Locale locale) {
        this.branding = branding;
        this.locale = locale;
    }

    public static String[] getISOLanguages() {
        return Locale.getISOLanguages();
    }

    public static String[] getISOCountries() {
        return Locale.getISOCountries();
    }
    
    public static Locale[] getAvailableLocales() {
        return Locale.getAvailableLocales();
    }
    
    public String getBranding() {
        return branding;
    }

    public void setBranding(String branding) {
        this.branding = branding;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public String getVariant() {
        return locale.getVariant();
    }

    public String getLanguage() {
        return locale.getLanguage();
    }

    public String getISO3Language() throws MissingResourceException {
        return locale.getISO3Language();
    }

    public String getISO3Country() throws MissingResourceException {
        return locale.getISO3Country();
    }

    public String getDisplayVariant(Locale inLocale) {
        return locale.getDisplayVariant(inLocale);
    }

    public final String getDisplayVariant() {
        return locale.getDisplayVariant();
    }

    public String getDisplayName(Locale inLocale) {
        return locale.getDisplayName(inLocale);
    }

    public final String getDisplayName() {
        return locale.getDisplayName();
    }

    public String getDisplayLanguage(Locale inLocale) {
        return locale.getDisplayLanguage(inLocale);
    }

    public final String getDisplayLanguage() {
        return locale.getDisplayLanguage();
    }

    public String getDisplayCountry(Locale inLocale) {
        return locale.getDisplayCountry(inLocale);
    }

    public String getCountry() {
        return locale.getCountry();
    }
    
    public String getBrandedDisplayName(String defaultLocaleText) {
        StringBuilder sb = new StringBuilder();
        if( hasBranding() ) {
            sb.append(branding).append(", ");
        }
        if(defaultLocaleText != null && locale == DEFAULT_FALLBACK){
            sb.append(defaultLocaleText);
        } else {
            sb.append(locale.getDisplayName());
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BrandedLocale other = (BrandedLocale) obj;
        if ((this.branding == null) ? (other.branding != null) : !this.branding.equals(other.branding)) {
            return false;
        }
        if (this.locale != other.locale && (this.locale == null || !this.locale.equals(other.locale))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 11 * hash + (this.branding != null ? this.branding.hashCode() : 0);
        hash = 11 * hash + (this.locale != null ? this.locale.hashCode() : 0);
        return hash;
    }
    
    

    @Override
    public String toString() {
        return getBrandedDisplayName(null);
    }
    
    

    public boolean hasBranding() {
        return notNullOrEmpty(branding);
    }

    public boolean hasLanguage() {
        return notNullOrEmpty(locale.getLanguage());
    }
    
    public boolean hasCountry() {
        return notNullOrEmpty(locale.getCountry());
    }
    
    public boolean hasVariant() {
        return notNullOrEmpty(locale.getVariant());
    }
    
    private boolean notNullOrEmpty(String value){
        return value != null && !value.trim().isEmpty();
    }

    public String getFilePart() {
        StringBuilder sb = new StringBuilder();
        if( hasBranding() ) {
            sb.append("_").append(branding);
        }
        
        if( hasLanguage() ) {
            sb.append("_").append(getLanguage());
        }
        
        if( hasCountry() ) {
            sb.append("_").append(getCountry());
        }
        if( hasVariant() ) {
            sb.append("_").append(getVariant());
        }
        
        return sb.toString();
    }

    
}
