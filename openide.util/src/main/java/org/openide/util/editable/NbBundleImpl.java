package org.openide.util.editable;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import org.openide.util.NbBundle;

/**
 *
 * @author Stefan
 */
public class NbBundleImpl implements NbResourceBundler{


    
    @Override
    public String getBranding() {
        return NbBundle.getBranding();
    }

    @Override
    public void setBranding(String bt) throws IllegalArgumentException {
        NbBundle.setBranding(bt);
    }

    @Override
    public ResourceBundle getBundle(String baseName, Locale locale, ClassLoader loader) throws MissingResourceException {
        return NbBundle.getBundle(baseName, locale, loader);
    }

    
}
