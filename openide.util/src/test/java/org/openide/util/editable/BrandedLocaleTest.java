/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openide.util.editable;

import java.util.Locale;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author stefan
 */
public class BrandedLocaleTest {
    
    public BrandedLocaleTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }

    @Test
    public void testGetFilePart() {
        
        BrandedLocale instance = new BrandedLocale();
        assertEquals("", instance.getFilePart());
        
        instance = new BrandedLocale("foo");
        assertEquals("_foo", instance.getFilePart());
        
        instance = new BrandedLocale(Locale.ENGLISH);
        assertEquals("_en", instance.getFilePart());
        
        instance = new BrandedLocale(Locale.UK);
        assertEquals("_en_GB", instance.getFilePart());
        
        instance = new BrandedLocale("bar", Locale.UK);
        assertEquals("_bar_en_GB", instance.getFilePart());
    }
}
