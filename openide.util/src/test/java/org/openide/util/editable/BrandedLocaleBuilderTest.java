package org.openide.util.editable;

import org.openide.util.editable.BrandedLocaleBuilder;
import org.openide.util.editable.BrandedLocale;
import java.util.Locale;
import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author Stefan
 */
public class BrandedLocaleBuilderTest {
    
    
    @Test
    public void testCreateBrandedLocale_default() {
        BrandedLocale expResult = new BrandedLocale();
        BrandedLocale result = BrandedLocaleBuilder.parse("Bundle");
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCreateBrandedLocale_withFileExtention() {
        BrandedLocale expResult = new BrandedLocale();
        BrandedLocale result = BrandedLocaleBuilder.parse("Bundle.properties");
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCreateBrandedLocale_brandedDefault() {
        BrandedLocale expResult = new BrandedLocale("myide", BrandedLocale.DEFAULT_FALLBACK);
        BrandedLocale result = BrandedLocaleBuilder.parse("Bundle_myide");
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCreateBrandedLocale_language() {
        BrandedLocale expResult = new BrandedLocale(null, new Locale("sv"));
        BrandedLocale result = BrandedLocaleBuilder.parse("Bundle_sv");
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCreateBrandedLocale_brandedLanguage() {
        BrandedLocale expResult = new BrandedLocale("myide", new Locale("sv"));
        BrandedLocale result = BrandedLocaleBuilder.parse("Bundle_myide_sv");
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCreateBrandedLocale_languageCountry() {
        BrandedLocale expResult = new BrandedLocale(null, new Locale("sv", "SE"));
        BrandedLocale result = BrandedLocaleBuilder.parse("Bundle_sv_SE");
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCreateBrandedLocale_brandedLanguageCountry() {
        BrandedLocale expResult = new BrandedLocale("myide", new Locale("sv", "SE"));
        BrandedLocale result = BrandedLocaleBuilder.parse("Bundle_myide_sv_SE");
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCreateBrandedLocale_languageCountryVariant() {
        BrandedLocale expResult = new BrandedLocale(null, new Locale("sv", "SE", "foo"));
        BrandedLocale result = BrandedLocaleBuilder.parse("Bundle_sv_SE_foo");
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCreateBrandedLocale_brandedLLanguageCountryVariant() {
        BrandedLocale expResult = new BrandedLocale("myide", new Locale("sv", "SE", "foo"));
        BrandedLocale result = BrandedLocaleBuilder.parse("Bundle_myide_sv_SE_foo");
        assertEquals(expResult, result);
    }
    
    @Test
    public void testCreateBrandedLocale_brandedLLanguageCountryVariant_withFileExtention() {
        BrandedLocale expResult = new BrandedLocale("myide", new Locale("sv", "SE", "foo"));
        BrandedLocale result = BrandedLocaleBuilder.parse("Bundle_myide_sv_SE_foo.properties");
        assertEquals(expResult, result);
    }
    
    
    
    @Test(expected=IllegalArgumentException.class)
    public void testCreateBrandedLocale_Overflow() {
        BrandedLocaleBuilder.parse("Bundle_sv_SE_foo_explode");
    }
    
    @Test(expected=IllegalArgumentException.class)
    public void testCreateBrandedLocale_brandedOverflow() {
        BrandedLocaleBuilder.parse("Bundle_myide_sv_SE_foo_explode");
    }
    
    
    @Test(expected=IllegalArgumentException.class)
    @Ignore(value="TwoLetterBranding is a known corner case")
    public void testTwoLetterBranding() {
        BrandedLocaleBuilder.parse("Bundle_my");
        BrandedLocaleBuilder.parse("Bundle_my_sv");
        BrandedLocaleBuilder.parse("Bundle_my_sv_SE");
        BrandedLocaleBuilder.parse("Bundle_my_sv_SE_foo");
    }
}
