package se.softstuff.nb.nbtranslate.marker;

import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author stefan
 */
public class MarkerUtilitiesTest {
    
    
    private String path;
    private String linuxPath;
    
    @Before
    public void setUp() {
        path = "file:/C:/work/guicomponentinfo~svn/GuiComponentInfoDemo/trunk/application/target/guicomponentinfodemo/guicomponentinfodemo/modules/se-softstuff-nb-GuiComponentInfoDemo-TopComponet.jar!/se/softstuff/nb/guicomponentinfodemo/Bundle_soft_sv_SE.properties";
        linuxPath = "file:/home/work/guicomponentinfo~svn/GuiComponentInfoDemo/trunk/application/target/guicomponentinfodemo/guicomponentinfodemo/modules/se-softstuff-nb-GuiComponentInfoDemo-TopComponet.jar!/se/softstuff/nb/guicomponentinfodemo/Bundle_soft_sv_SE.properties";
        
    }

    
    @Test
    public void locateModuleRef(){
        String expResult = "se-softstuff-nb-GuiComponentInfoDemo-TopComponet";
        String result = MarkerUtilities.locateModuleRef(path);
        assertEquals(expResult, result);
    }

    @Test
    public void locateJarFileName() {
        String expResult = "se-softstuff-nb-GuiComponentInfoDemo-TopComponet.jar";
        String result = MarkerUtilities.locateJarFileName(path);
        assertEquals(expResult, result);
    }

    @Test
    public void locateJarFile() {
        String expResult = "C:/work/guicomponentinfo~svn/GuiComponentInfoDemo/trunk/application/target/guicomponentinfodemo/guicomponentinfodemo/modules/se-softstuff-nb-GuiComponentInfoDemo-TopComponet.jar";
        String result = MarkerUtilities.locateJarFile(path);
        assertEquals(expResult, result);
    }
    

    @Test
    public void locateJarFileInLinux() {
        String expResult = "/home/work/guicomponentinfo~svn/GuiComponentInfoDemo/trunk/application/target/guicomponentinfodemo/guicomponentinfodemo/modules/se-softstuff-nb-GuiComponentInfoDemo-TopComponet.jar";
        String result = MarkerUtilities.locateJarFile(linuxPath);
        assertEquals(expResult, result);
    }

    @Test
    public void locateResourcePath() {
        String expResult = "se/softstuff/nb/guicomponentinfodemo/";
        String result = MarkerUtilities.locateResourcePath(path);
        assertEquals(expResult, result);
    }
    
    @Test
    public void locateResourcePath_notFromJar() {
        String expResult = "se/softstuff/nb/guicomponentinfodemo/";
        String result = MarkerUtilities.locateResourcePath("se/softstuff/nb/guicomponentinfodemo/Bundle_soft_sv_SE.properties");
        assertEquals(expResult, result);
    }
    
    @Test
    public void locateResourceFile(){
        String expResult = "se/softstuff/nb/guicomponentinfodemo/Bundle_soft_sv_SE.properties";
        String result = MarkerUtilities.locateResourceFile(path);
        assertEquals(expResult, result);
    }
    
    @Test
    public void getBundleBrandingAndLocaleString(){
        String expected = "soft_sv_SE";
        String actual = MarkerUtilities.getBundleBrandingAndLocaleString(path);
        assertEquals(expected, actual);
    }
    
    @Test
    public void doResourceMatchSelectedBunlde() {
        
        String resource = "se/softstuff/nb/guicomponentinfodemo/";
        
        String name = "se/softstuff/nb/guicomponentinfodemo/Bundle.properties";        
        assertTrue(MarkerUtilities.doResourceMatchSelectedBunlde(name, resource));
        
        name = "se/softstuff/nb/guicomponentinfodemo/Bundle_en_US.properties";
        assertTrue(MarkerUtilities.doResourceMatchSelectedBunlde(name, resource));
        
        name = "se/softstuff/nb/guicomponentinfodemo/Bundle_gurkan_en_US.properties";
        assertTrue(MarkerUtilities.doResourceMatchSelectedBunlde(name, resource));
        
        name = "se/softstuff/nb/Bundle.properties";
        assertFalse(MarkerUtilities.doResourceMatchSelectedBunlde(name, resource));
        
        name = "se/softstuff/nb/guicomponentinfodemo/Foo.class";
        assertFalse(MarkerUtilities.doResourceMatchSelectedBunlde(name, resource));
    }
   
    @Test
    public void locateBundleTypes() {
        List<String> bundles = Arrays.asList("se/softstuff/nb/guicomponentinfodemo/Bundle.properties", "se/softstuff/nb/guicomponentinfodemo/Bundle_sv_SE.properties", "se/softstuff/nb/guicomponentinfodemo/Bundle_branding_sv_SE.properties");
        List<String> bundleTypes = MarkerUtilities.locateBundleTypes(bundles);
        
        assertEquals(3, bundleTypes.size());
        assertTrue(bundleTypes.contains(""));
        assertTrue(bundleTypes.contains("sv_SE"));
        assertTrue(bundleTypes.contains("branding_sv_SE"));
        
        
    }
}

