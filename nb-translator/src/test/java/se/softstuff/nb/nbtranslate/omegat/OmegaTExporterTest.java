package se.softstuff.nb.nbtranslate.omegat;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mats
 */
public class OmegaTExporterTest {
    
    private OmegaTExporter instance;
    
    @Before
    public void setup() {
        instance = new OmegaTExporter(null, null, true);
        instance.setRelevantLocale("sv_SE");
    }
    
    
    @Test
    public void resourceIsRelevant() {
        assertTrue(instance.resourceIsRelevant("se.databyran.prosang.client.core.program.spi.Bundle_sv_SE.properties"));
        assertFalse(instance.resourceIsRelevant("se.databyran.prosang.client.core.program.spi.Bundle_no_NO.properties"));
    }
    
    @Test
    public void resourceIsRelevantWorksForDefaultLocale() {
        instance.setRelevantLocale(null);
        assertTrue(instance.resourceIsRelevant("se.databyran.prosang.client.core.program.spi.Bundle.properties"));
        assertFalse(instance.resourceIsRelevant("se.databyran.prosang.client.core.program.spi.Bundle_no_NO.properties"));
    }

}
