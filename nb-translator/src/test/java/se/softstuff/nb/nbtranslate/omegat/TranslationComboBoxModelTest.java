package se.softstuff.nb.nbtranslate.omegat;

import java.util.Arrays;
import javax.swing.ComboBoxModel;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import se.softstuff.nb.nbtranslate.data.NbTranslation;

/**
 *
 * @author mats
 */
public class TranslationComboBoxModelTest {

    private TranslationComboBoxModel instance;

    private NbTranslation translation;

    @Before
    public void setup() {
        translation = new NbTranslation();
        translation.setBundleLocales(Arrays.asList("sv_SE", "junit_CA"));
        instance = new TranslationComboBoxModel(translation);
    }

    @Test
    public void generateComboBoxModelGivesLocalesInModel() {
        assertEquals(2, instance.getSize());
    }
}
