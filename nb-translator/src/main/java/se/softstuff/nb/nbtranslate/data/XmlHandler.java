/*
 * Copyright 2012 ztefan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.softstuff.nb.nbtranslate.data;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

public class XmlHandler {

    private static final String TOP = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    public static final String UTF8 = "UTF8";
    

    public static String toXml(NbTranslation sot) {
        StringWriter out = new StringWriter();
        toXml(sot, out);
        String xml = out.toString();
        System.out.println(xml);
        return xml;
    }

    public static void toXml(NbTranslation sot, Writer out) {
        try {
            out.write(TOP);
            XStream xstream = buildXStream();
            xstream.toXML(sot, out);
        } catch (IOException ex) {
            throw new RuntimeException("Failed to write xml", ex);
        }
    }

    public static void toXml(NbTranslation sot, OutputStream out) {
        try {
            Writer writer = new OutputStreamWriter(out, UTF8);
            toXml(sot, writer);
        } catch (IOException ex) {
            throw new RuntimeException("Failed to write xml", ex);
        }
    }

    public static NbTranslation fromXml(String xml) {
        Reader reader = new StringReader(xml);
        return fromXml(reader);
    }
    
    public static NbTranslation fromXml(InputStream xml) {
        try {
            Reader reader = new InputStreamReader(xml, UTF8);
            return fromXml(reader);
        } catch (IOException ex) {
            throw new RuntimeException("Failed to write xml", ex);
        }
    }

    public static NbTranslation fromXml(Reader xml) {
        XStream xstream = buildXStream();
        NbTranslation sot = (NbTranslation) xstream.fromXML(xml, new NbTranslation());
        return sot;
    }

    private static XStream buildXStream() {
        XStream xstream = new XStream(new DomDriver());
        xstream.alias("netbeans-translate-project", NbTranslation.class);
        xstream.useAttributeFor(NbTranslation.class, "xmlns");
        xstream.alias("project", ProjectBundleInfo.class);
        return xstream;
    }

}
