/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.softstuff.nb.nbtranslate.manager.node;

/**
 *
 * @author Stefan
 */
class Resource {

    private String module;
    private String resource;

    public Resource(String module, String resource) {
        this.module = module;
        this.resource = resource;
    }

    public String getModule() {
        return module;
    }

    public String getResource() {
        return resource;
    }
}
