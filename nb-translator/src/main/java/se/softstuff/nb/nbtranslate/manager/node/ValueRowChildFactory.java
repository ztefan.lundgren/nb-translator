/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.softstuff.nb.nbtranslate.manager.node;

import java.beans.IntrospectionException;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import se.softstuff.nb.nbtranslate.data.NbTranslation;

/**
 *
 * @author Stefan
 */
class ValueRowChildFactory extends ChildFactory<ValueRow> implements PropertyChangeListener{

    private Resource resource;
    private NbTranslation translation;
    
    ValueRowChildFactory(Resource resource, NbTranslation translation) {
        this.resource = resource;
        this.translation=translation;
    }

    @Override
    protected boolean createKeys(List<ValueRow> toPopulate) {
        
        Map<String, Properties> originalResource = translation.getOriginalResource(resource.getModule());
        Map<String, Properties> changedResource = translation.getChangedResource(resource.getModule());
        if(changedResource==null){
            changedResource = new HashMap<String, Properties>();
        }
        
        Properties origin = originalResource.get(resource.getResource());
        Properties changed = changedResource.get(resource.getResource());
        if(changed==null){
            changed = new Properties();
        }
        if(origin==null){
            origin = new Properties();
        }
        
        List<String> originKeys = new ArrayList<String>(origin.stringPropertyNames());
        List<String> chagedKeys = new ArrayList<String>(changed.stringPropertyNames());
        
        for(String key : originKeys){
            String originValue = origin.getProperty(key, "");
            String changedValue = changed.getProperty(key, "");
            
            ValueRow row = new ValueRow(resource.getModule(), resource.getResource(), key, originValue, changedValue);
            row.addPropertyChangeListener(this);
            toPopulate.add(row);
            chagedKeys.remove(key);
        }
        
        for(String key : chagedKeys){
            String originValue = origin.getProperty(key, "");
            String changedValue = changed.getProperty(key, "");
            
            ValueRow row = new ValueRow(resource.getModule(), resource.getResource(), key, originValue, changedValue);
            row.addPropertyChangeListener(this);
            toPopulate.add(row);
        }
        
        
        return true;
    }

    @Override
    protected Node createNodeForKey(ValueRow key) {
        Node node = null;
        try {
            node = new ValueRowNode(key);
        } catch (IntrospectionException ex) {
            Exceptions.printStackTrace(ex);
        }
        return node;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        ValueRow row = (ValueRow)evt.getSource();
        translation.addResourceChange(row.getModule(),row.getResouce(), row.getKey(), row.getChanged());
    }

    
}
