package se.softstuff.nb.nbtranslate.marker;

import java.awt.Component;
import java.awt.Window;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.logging.Logger;
import javax.swing.RootPaneContainer;

/**
 * Keeps track of the active window and adds the glasspane to the new window.<br>
 * Typical senario is to jump to a dialog and put the glasspane on it and back again.
 * @author stefan
 */
public class ActiveWindowTracker implements WindowListener {

    public static final String PROP_ACTIVEWINDOW = "activeWindow"; //NOI18N

    private Component oldGlassPane;

    private Component gluedGlasspane;

    private Window activeWindow;


    public Window getActiveWindow() {
        return activeWindow;
    }
    public RootPaneContainer getRootPaneContainer(){
        return (RootPaneContainer)activeWindow;
    }

    public void setActiveWindow(Window activeWindow) {
        Window oldActiveWindow = this.activeWindow;
        this.activeWindow = activeWindow;
        propertyChangeSupport.firePropertyChange(PROP_ACTIVEWINDOW, oldActiveWindow, activeWindow);
    }
    private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    public void glueGlassPane(Window window, Component newGlasspane) {
        if (window == null || activeWindow == window) {
            return;
        }
        if (newGlasspane == null) {
            throw new IllegalArgumentException("newGlasspane cant be null"); // NOI18N
        }
        if (activeWindow != null) {
            activeWindow.removeWindowListener(this);
            
        }
        if(oldGlassPane!=null){
            getRootPaneContainer().setGlassPane(oldGlassPane);
            oldGlassPane.setVisible(false);
        }
        RootPaneContainer rpc = (RootPaneContainer)window;
        oldGlassPane = rpc.getGlassPane();
        rpc.setGlassPane(newGlasspane);
        
        gluedGlasspane = newGlasspane;
        window.addWindowListener(this);
        
        setActiveWindow(window);
        gluedGlasspane.setVisible(true);
        Logger.getLogger(this.getClass().getName()).info("glueGlassPane newGlasspane " + newGlasspane.getName() + " " + newGlasspane.getClass().getSimpleName() + " to " + activeWindow.getName() + " " + activeWindow.getClass().getSimpleName()); //NOI18N
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
        
        glueGlassPane(e.getWindow(), gluedGlasspane);
    }

    @Override
    public void windowDeactivated(WindowEvent e) {

        glueGlassPane(e.getOppositeWindow(), gluedGlasspane);

    }
    

    void stop() {
        if(activeWindow!=null){
            activeWindow.removeWindowListener(this);
        }
    }
}
