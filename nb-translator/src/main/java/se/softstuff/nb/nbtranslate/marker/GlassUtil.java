package se.softstuff.nb.nbtranslate.marker;

import java.awt.Component;
import java.awt.Container;
import java.awt.Point;
import java.awt.event.MouseEvent;
import javax.swing.JRootPane;
import javax.swing.RootPaneContainer;
import javax.swing.SwingUtilities;

public class GlassUtil {
    
    
    public static Component findComponentUnderGlassPaneAt(MouseEvent mouseEvent) {
         return findComponentUnderGlassPaneAt(mouseEvent.getComponent(), mouseEvent.getPoint());
     }
    
    public static Component findComponentUnderGlassPaneAt(Component top, Point onComp) {
       
        Component c;

        if( top instanceof HighlighterGlass) {
            HighlighterGlass glass = (HighlighterGlass)top;
            c = glass.getComponentAtGlasPanePoint(onComp);
//            Container contentPane = rootPane.getContentPane();
//            Point convertPoint = SwingUtilities.convertPoint(top, onComp, contentPane);
//            c = contentPane.findComponentAt(convertPoint);
        } else if (top instanceof RootPaneContainer ) {
            c = ((RootPaneContainer) top).getLayeredPane().findComponentAt(
                    SwingUtilities.convertPoint(top, onComp, ((RootPaneContainer) top).getLayeredPane()));
        } else {
            c = ((Container) top).findComponentAt(onComp);
        }
        

        return c;
    }
}
