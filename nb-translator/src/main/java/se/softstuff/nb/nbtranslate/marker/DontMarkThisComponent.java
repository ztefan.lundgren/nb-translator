package se.softstuff.nb.nbtranslate.marker;

/**
 * Implement this interface to exclude tracking form a component
 * @author stefan
 */
public interface DontMarkThisComponent {
    
}
