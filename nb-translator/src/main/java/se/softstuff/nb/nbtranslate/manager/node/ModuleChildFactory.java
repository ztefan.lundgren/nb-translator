/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.softstuff.nb.nbtranslate.manager.node;

import java.beans.IntrospectionException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import se.softstuff.nb.nbtranslate.data.NbTranslation;

/**
 *
 * @author Stefan
 */
public class ModuleChildFactory  extends ChildFactory<Module> {
    
    
    private NbTranslation translation;
    
    public ModuleChildFactory( NbTranslation translation){
        this.translation = translation;
    }

    @Override
    protected boolean createKeys(List<Module> toPopulate) {
        List<String> changedModulesNames = new ArrayList<String>(translation.getChangedModulesNames());
        for(String module : translation.getOriginalModulesNames()){
            toPopulate.add(new Module(module));
            changedModulesNames.remove(module);
        }
        
        for(String module :changedModulesNames){
            toPopulate.add(new Module(module));
        }
        return true;
    }

    @Override
    protected Node createNodeForKey(Module key) {
        Node node = new ModuleNode(key, translation);
        return node;
    }
    
}
