/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.softstuff.nb.nbtranslate.manager.node;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import se.softstuff.nb.nbtranslate.data.NbTranslation;

/**
 *
 * @author Stefan
 */
class ResourceChildFactory extends ChildFactory<Resource> {

    private Module module;

    private NbTranslation translation;

    public ResourceChildFactory(Module module, NbTranslation translation) {
        this.module = module;
        this.translation = translation;
    }

    @Override
    protected boolean createKeys(List<Resource> toPopulate) {
        Map<String, Properties> changedResource = translation.getChangedResource(module.getModule());
        List<String> changedKeys = changedResource != null ? new ArrayList<String>(changedResource.keySet()) : Collections.EMPTY_LIST;

        for (String resource : translation.getOriginalResource(module.getModule()).keySet()) {
            toPopulate.add(new Resource(module.getModule(), resource));
            changedKeys.remove(resource);
        }

        for (String resource : changedKeys) {
            toPopulate.add(new Resource(module.getModule(), resource));
        }
        return true;
    }

    @Override
    protected Node createNodeForKey(Resource key) {
        Node node = new ResourceNode(key, translation);
        return node;
    }
}
