package se.softstuff.nb.nbtranslate.manager;

import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import se.softstuff.nb.nbtranslate.data.NbTranslation;
import se.softstuff.nb.nbtranslate.marker.MarkerUtilities;

/**
 *
 * @author stefan
 */
public class SearchPressentationModel {

    private static final String EMPTY = "";
    private NbTranslation content;
    
    void init(NbTranslation content) {
        this.content = content;
    }

    Set<SearchElementResultRow> search(String textToFind) {
        
        Pattern pattern = Pattern.compile(textToFind);
        
        boolean isOriginal = false;
        Set<SearchElementResultRow> result = new HashSet<SearchElementResultRow>();
        findRows(pattern, isOriginal, result);
        isOriginal = true;
        findRows(pattern, isOriginal, result);
        
        return result;
    }

    private int findRows(Pattern pattern, boolean isOriginal, Set<SearchElementResultRow> result) {
        Set<String> modules = getModules(isOriginal);
        int fileCount = 0;
        for(String module : modules){
            Map<String, Properties> changedResource = getResource(module,isOriginal);
            for(String resource : changedResource.keySet()){
                fileCount++;
                String language = MarkerUtilities.getBundleBrandingAndLocaleString(resource);
                Properties props = changedResource.get(resource);
                for(String key : props.stringPropertyNames()){
                    String original = content.getOriginalValue(module,resource,key, EMPTY);
                    String changed = content.getChangedValue(module,resource,key, EMPTY);
                    
                    Matcher matcherOriginal = pattern.matcher(original);
                    Matcher matcherChanged = pattern.matcher(changed);
                    if(matcherOriginal.find() || matcherChanged.find()){
                        SearchElementResultRow row;
                        row = new SearchElementResultRow(key, original, changed, language, module, resource);
                        result.add(row);
                    }
                }
            }
        }
        return fileCount;
    }
    
    private Set<String> getModules(boolean isOriginal){
        return isOriginal ? content.getOriginalModulesNames() : content.getChangedModulesNames();
    }
    
    private Map<String, Properties>getResource(String module, boolean isOriginal){
        return isOriginal ? content.getOriginalResource(module) : content.getChangedResource(module);
    }

    void editRow(SearchElementResultRow row, String newValue) {
        content.addResourceChange(row.getModule(), row.getResource(), row.getKey(), newValue);
        row.setValue(newValue);
    }
    
}
