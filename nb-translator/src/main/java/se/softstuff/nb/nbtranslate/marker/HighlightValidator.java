package se.softstuff.nb.nbtranslate.marker;

import java.awt.Component;

/**
 * Validator if the component is to be highlighted
 * 
 * @author stefan
 */
public interface HighlightValidator {
    
    /**
     * Validate the component
     * @param component
     * @return is ok to highlighted component
     */
    boolean validate(Component component);
        
}
