package se.softstuff.nb.nbtranslate.manager;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import org.openide.awt.StatusDisplayer;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Exceptions;
import org.openide.windows.WindowManager;

public final class OpenFileAction implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
         JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(createFileFilter());
        int returnVal = fileChooser.showOpenDialog(WindowManager.getDefault().getMainWindow());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                File file = fileChooser.getSelectedFile();
                if (!file.exists()) {
                    JOptionPane.showMessageDialog(fileChooser, "File " + file.getName() + " does not exist", "Unable to find file", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                FileObject f = FileUtil.toFileObject(file);
                DataObject find = DataObject.find(f);
                OpenCookie open = find.getLookup().lookup(OpenCookie.class);
                open.open();

            } catch (DataObjectNotFoundException ex) {
                Exceptions.printStackTrace(ex);
            }


        } else {
            System.out.println("File access cancelled by user.");
        }
    }


    private FileFilter createFileFilter() {
        return new FileFilter() {

            @Override
            public boolean accept(File f) {
                return f.getName().endsWith(".nbt") || f.isDirectory();
            }

            @Override
            public String getDescription() {
                return "NbTranslator file (*.nbt)";
            }

        };
    }

}
