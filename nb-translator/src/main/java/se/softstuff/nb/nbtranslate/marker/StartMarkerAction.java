/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.softstuff.nb.nbtranslate.marker;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public final class StartMarkerAction implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        TracerManager instance = TracerManager.getInstance();
        instance.toggleTracker();
    }
}
