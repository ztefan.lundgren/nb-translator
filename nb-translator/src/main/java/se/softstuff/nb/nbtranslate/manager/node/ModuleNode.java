/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.softstuff.nb.nbtranslate.manager.node;

import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import se.softstuff.nb.nbtranslate.data.NbTranslation;

/**
 *
 * @author Stefan
 */
public class ModuleNode extends AbstractNode {
    
    public ModuleNode(Module module, NbTranslation translation) {
        super(Children.create(new ResourceChildFactory(module,translation), false));
        setName(module.getModule());
        setDisplayName(module.getModule());
    }
}
