package se.softstuff.nb.nbtranslate.omegat;

import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import se.softstuff.nb.nbtranslate.data.NbTranslation;

/**
 *
 * @author mats
 */
final class TranslationComboBoxModel extends DefaultComboBoxModel {
    

    public TranslationComboBoxModel(NbTranslation translation){
        super(getBundleLocales(translation));
    }

    

    private static String[] getBundleLocales(NbTranslation translation) {
        List<String> locales = translation.getBundleLocales();
        
        return locales.toArray(new String[locales.size()]);
    }
    
}
