/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.softstuff.nb.nbtranslate.manager.node;

import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import se.softstuff.nb.nbtranslate.data.NbTranslation;

/**
 *
 * @author Stefan
 */
class ResourceNode extends AbstractNode {
    public ResourceNode(Resource resource, NbTranslation translation){
        super(Children.create(new ValueRowChildFactory(resource,translation), false));
        setName(resource.getResource());
        setDisplayName(resource.getResource());
    }
}
