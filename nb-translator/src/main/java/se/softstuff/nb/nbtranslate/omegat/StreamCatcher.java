package se.softstuff.nb.nbtranslate.omegat;

import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author stefan
 */
public class StreamCatcher  extends Thread {

    private InputStream is;
    

    StreamCatcher(InputStream is) {
        this.is = is;
    }

    @Override
    public void run() {
        try {
            while(is.read()!=-1){
                ;
            }
            
        } catch (IOException ioe) {
            ;
        } finally {
            if (is != null) {
                try {
                is.close();
                } catch (IOException ex) {
                    ;
                }
            }
        }
    }
}
