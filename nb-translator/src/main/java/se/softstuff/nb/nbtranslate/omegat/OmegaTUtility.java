package se.softstuff.nb.nbtranslate.omegat;

import se.softstuff.nb.nbtranslate.util.EncodingUtil;
import java.io.IOException;
import java.util.List;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import se.softstuff.nb.nbtranslate.data.NbTranslation;
import static se.softstuff.nb.nbtranslate.omegat.DirectoryUtil.*;

/**
 *
 * @author Stefan
 */
public class OmegaTUtility {

    public static int importChanges(NbTranslation translation, FileObject projectDir) throws IOException {
        
        int rowsUpdated=0;
        FileLock lock = projectDir.lock();
        try {
            FileObject tartetDir = getChildDir(projectDir, "target");
            for(FileObject moduleFile : tartetDir.getChildren()){
                String module = moduleFile.getName();
                for(FileObject resourceFile : moduleFile.getChildren()){
                    String resource = resourceFile.getName()+".properties";
                    
                    List<String> lines = resourceFile.asLines(EncodingUtil.ENCODING);
                    for(String line : lines){
                        line = EncodingUtil.loadConvert(line);
                        int eq = line.indexOf("=");
                        if(eq == -1){
                            continue;
                        }
                        String key = line.substring(0, eq);
                        String value = line.substring(eq+1);
                        translation.addResourceChange(module, resource, key, value);  
                        rowsUpdated++;
                    }
                }
            }
            return rowsUpdated;
        } finally {
            lock.releaseLock();
        }
    }
    
    
    
}
