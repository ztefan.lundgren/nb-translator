package se.softstuff.nb.nbtranslate.data;

import org.openide.cookies.OpenCookie;
import se.softstuff.nb.nbtranslate.manager.ManagerTopComponent;

/**
 *
 * @author stefan
 */
public class NbtOpenCookie implements OpenCookie {

    private NbTranslationDataObject translatorFile;

    public NbtOpenCookie(NbTranslationDataObject translatorFile) {
        this.translatorFile = translatorFile;
    }
    
    
    @Override
    public void open() {
        
            
        ManagerTopComponent instance = ManagerTopComponent.findInstance();
        if(instance.loadDataObject(translatorFile)) {
            instance.open();
            instance.requestActive();
        }
    }
    
}
