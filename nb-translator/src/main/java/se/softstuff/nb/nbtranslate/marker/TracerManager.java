package se.softstuff.nb.nbtranslate.marker;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JWindow;
import javax.swing.KeyStroke;
import javax.swing.RootPaneContainer;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.Utilities;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.windows.WindowManager;

/**
 * Start and stops the tracer. Keeps the lookup used to send all component
 * updates
 *
 * @author stefan
 */
public class TracerManager {

    public static final String PROP_TRACKER_ENABLED = "TrackerEnabled"; // NOI18N
    
    private static final TracerManager INSTANCE = new TracerManager();
    
    public static TracerManager getInstance(){
        return INSTANCE;
    }
    
    private final PropertyChangeSupport propertyChangeSupport;
    private final Lookup lookup;
    private final InstanceContent ic;
    private final List<Color> colorStack;
    private transient boolean trackerEnabled;
    private HighlighterGlass highlighter;
    private Result<HighlightedComponent> lookupResult;
    Set<Class> excluded = new HashSet<Class>();
    private boolean paused;
    private final Logger log = Logger.getLogger(getClass().getName());
//    private StatStopTracerEventListener statStopListener;
    

    private TracerManager() {
        log.fine("TracerManagerImpl()");
        propertyChangeSupport = new PropertyChangeSupport(this);
        colorStack = new ArrayList(Arrays.asList(Color.RED, Color.YELLOW, Color.PINK, Color.CYAN, Color.MAGENTA, Color.BLUE, Color.GREEN));

        ic = new InstanceContent();
        lookup = new AbstractLookup(ic);
        lookupResult = lookup.lookupResult(HighlightedComponent.class);
    }

    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    
    public Lookup getLookup() {
        return lookup;
    }

    
    public boolean isTrackerEnabled() {
        return trackerEnabled;
    }

    private void setTracerEnabled(boolean enabled) {
        paused = false;
        propertyChangeSupport.firePropertyChange(PROP_TRACKER_ENABLED, trackerEnabled, trackerEnabled = enabled);
    }

    /**
     * starts tracker if stopped or stops it if its started
     */
    
    public synchronized void toggleTracker() {
        if (isTrackerEnabled()) {
            stopTracker();
        } else {
            startTracker();
        }
    }

    
    public synchronized void startTracker() {
        startTrackerAndMark(null);
    }
    
    public synchronized void startTrackerAndMark(final Component comp) {
        paused = false;
        if (trackerEnabled) {
            stopTracker();
//            return; //Tracker is alredy started
        }
        setTracerEnabled(true);


        WindowManager.getDefault().invokeWhenUIReady(new Runnable() {
            
            @Override
            public void run() {

                log.fine(String.format("Start component tracer")); // NOI18N

                highlighter = new HighlighterGlass(TracerManager.this);

                Window componentWindow = SwingUtilities.windowForComponent(comp);
                if(componentWindow == null || componentWindow instanceof RootPaneContainer){
                    highlighter.start((RootPaneContainer)componentWindow);
                } else {
                    log.log(Level.SEVERE, "componentWindow is not a RootPaneContainer: "+componentWindow);
                }
                
                lookupResult.addLookupListener(highlighter);

                if(comp!=null){
                    lit(comp, colorStack.get(0));
                }
                WindowManager.getDefault().updateUI();

            }
        });
    }
    
    
    public boolean isHighlighterPaused(){
        return paused;
    }
    
    
    public synchronized void toggleHighlighter() {
        paused = !paused;
    }

    
    public synchronized void pauseHighlighter() {
        paused = true;
    }

    
    public synchronized void resumeHighlighter() {
        paused = false;
    }

    
    public synchronized void stopTracker() {

        if (!trackerEnabled) {
            return;
        }
        log.fine("Stop component tracer"); // NOI18N
        setTracerEnabled(false);

        lookupResult.removeLookupListener(highlighter);

        highlighter.stop();

    }

    /**
     * Creates and puts the HiglightedComponent in the lookup
     *
     * @param color Handle missing or null color by ignoring and put first color
     * insted
     */
    
    public synchronized void lit(Component component, Color color) {
        if (!isTrackerEnabled() || paused) {
            return;
        }
        int index = color != null ? colorStack.indexOf(color) : 0;
        if (index < 0) {
            index = 0; // ignore missing color
        }
        ListIterator<Color> listIterator = colorStack.listIterator(index);
        HighlightedComponent item = HighlightedComponent.create(component, listIterator);
        if (item != null) {

            // Add the new item to the lookup
            ic.set(Arrays.asList(item), null);
        }
    }

    
//    public String getShortcutKeyDescription() {
//        return findOpenActionAccelerator(MouseTracerAction.class);
//    }

    // <editor-fold defaultstate="collapsed" desc="find the action accelerator keys">
    private static String findOpenActionAccelerator(Class action) {
        String clazz = action.getName().replaceAll("\\.", "-") + ".instance"; // NOI18N
        KeyStroke stroke = findKeyStrokeForShortcut(clazz);
        return getAcceleratorText(stroke);
    }

    private static KeyStroke findKeyStrokeForShortcut(String clazz) {
        for (FileObject key : FileUtil.getConfigFile("Shortcuts").getChildren()) { // NOI18N
            Object action = key.getAttribute("originalFile"); // NOI18N
            if (action instanceof String) {
                if (((String) action).endsWith(clazz)) {
                    String keycode = key.getName();
                    KeyStroke stroke = Utilities.stringToKey(keycode);
                    return stroke;
                }
            }
        }
        return null;
    }

    /**
     * converts KeyStroke to readable string format. this can be used to show
     * the accelerator along with the tooltip for toolbar buttons
     *
     * this code is extracted from javax.swing.plaf.basic.BasicMenuItemUI class
     */
    private static String getAcceleratorText(KeyStroke accelerator) {
        if (accelerator == null) {
            return "";// NOI18N
        }
        String acceleratorDelimiter = UIManager.getString("MenuItem.acceleratorDelimiter");// NOI18N
        if (acceleratorDelimiter == null) {
            acceleratorDelimiter = "+";// NOI18N
        }

        String acceleratorText = "";// NOI18N
        if (accelerator != null) {
            int modifiers = accelerator.getModifiers();
            if (modifiers > 0) {
                acceleratorText = KeyEvent.getKeyModifiersText(modifiers);
                acceleratorText += acceleratorDelimiter;
            }

            int keyCode = accelerator.getKeyCode();
            if (keyCode != 0) {
                acceleratorText += KeyEvent.getKeyText(keyCode);
            } else {
                acceleratorText += accelerator.getKeyChar();
            }
        }
        return acceleratorText;
    }
    // </editor-fold>

    
    
//    public void attachTrigger(TracerTriggerKeys startKeys) {
//        log.fine("attachTrigger");
//        if(statStopListener == null) {
//            statStopListener = new StatStopTracerEventListener(this);
//            Toolkit toolkit = Toolkit.getDefaultToolkit();
//            toolkit.addAWTEventListener(statStopListener, AWTEvent.MOUSE_EVENT_MASK);
//        }
//        
//        statStopListener.setStartTrigger(startKeys);
//    }
//
//    
//    public void detatchTrigger() {
//        log.fine("detatchTrigger");
//        if(statStopListener != null){
//            Toolkit toolkit = Toolkit.getDefaultToolkit();
//            toolkit.removeAWTEventListener(statStopListener);
//        }
//    }

    
}
