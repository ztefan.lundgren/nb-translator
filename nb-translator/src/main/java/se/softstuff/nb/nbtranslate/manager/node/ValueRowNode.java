/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.softstuff.nb.nbtranslate.manager.node;

import java.beans.IntrospectionException;
import org.openide.nodes.BeanNode;

/**
 *
 * @author Stefan
 */
class ValueRowNode extends BeanNode<Object> {

    public ValueRowNode(ValueRow bean) throws IntrospectionException {
        super(bean);
        setName(bean.getKey());
        setDisplayName(bean.getKey());
    }
    
}
