package se.softstuff.nb.nbtranslate.omegat;

import org.openide.filesystems.FileObject;

/**
 *
 * @author mats
 */
public class DirectoryUtil {
    
    /**
     * 
     * @param parent directory to search from 
     * @param nameOfChild name of sub directory to find.
     * @return the sub directory with the given name from the given parent.
     */
     public static FileObject getChildDir(FileObject parent, String nameOfChild) {
        for(FileObject child : parent.getChildren()){
            if(child.getNameExt().equalsIgnoreCase(nameOfChild)){
                return child;
            }
        }
        return null;
    }
}
