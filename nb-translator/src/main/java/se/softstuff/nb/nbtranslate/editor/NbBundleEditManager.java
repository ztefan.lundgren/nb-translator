package se.softstuff.nb.nbtranslate.editor;

import se.softstuff.nb.nbtranslate.data.NbTranslation;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.io.IOException;
import java.util.logging.Logger;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.*;
import org.openide.util.Lookup;
import org.openide.util.editable.NbBundle;
import se.softstuff.nb.nbtranslate.data.NbTranslationDataObject;
import se.softstuff.nb.nbtranslate.marker.TracerManager;

/**
 *
 * @author Stefan
 */
public class NbBundleEditManager implements PropertyChangeListener, VetoableChangeListener, FileChangeListener {

    private static final NbBundleEditManager INSTANCE = new NbBundleEditManager();

    public static NbBundleEditManager getInstance() {
        return INSTANCE;
    }
    private final Logger log = Logger.getLogger(getClass().getName());
    private NbTranslationDataObject data;
    private FileLock lock;
    private NbTranslation translation;
    private EditableBundler bundler;
    
   
    private NbBundleEditManager() {
    }

    public boolean isAttached() {
        return data != null;
    }

    public synchronized void attach(NbTranslationDataObject data) throws IOException {
        detach();
        
        log.fine("start begin");
//        lock = data.getPrimaryFile().lock();
        data.addPropertyChangeListener(this);
        data.addVetoableChangeListener(this);
        data.getPrimaryFile().addFileChangeListener(this);

        translation = data.getContent();

        bundler = new EditableBundler(translation);
        NbBundle.setBundler(bundler);
        this.data = data;
        
        translation.addPropertyChangeListener(this);
                
        log.fine("start done");

    }

    public synchronized void detach() throws IOException {

        log.fine("stop begin");
        
        if(data == null){
            return;
        }
        
        

        bundler = null;
        NbBundle.setStandardBundler();
        
        translation.removePropertyChangeListener(this);
        translation = null;

        data.removePropertyChangeListener(this);
        data.removeVetoableChangeListener(this);
        data.getPrimaryFile().removeFileChangeListener(this);
//        lock.releaseLock();
        data = null;
        
        TracerManager tracerManager = TracerManager.getInstance();
        tracerManager.removePropertyChangeListener(this);
        log.fine("start done");
    }

    public synchronized void saveToFile() throws IOException {
        data.getCookie(SaveCookie.class).save();
    }

        
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if(evt.getPropertyName().equals(NbTranslation.PROP_RESOURCE_CHANGES)){
            bundler.clearCache(); // some thing changed clear cache to be in syc
        }
        log.fine("propertyChange " + evt);
    }

    @Override
    public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
        log.fine("vetoableChange " + evt);
    }

    @Override
    public void fileFolderCreated(FileEvent fe) {
        log.fine("fileFolderCreated " + fe);
    }

    @Override
    public void fileDataCreated(FileEvent fe) {
        log.fine("fileDataCreated " + fe);
    }

    @Override
    public void fileChanged(FileEvent fe) {
        log.fine("fileChanged " + fe);
    }

    @Override
    public void fileDeleted(FileEvent fe) {
        log.fine("fileDeleted " + fe);
    }

    @Override
    public void fileRenamed(FileRenameEvent fe) {
        log.fine("fileRenamed " + fe);
    }

    @Override
    public void fileAttributeChanged(FileAttributeEvent fe) {
        log.fine("fileAttributeChanged " + fe);
    }

    NbTranslation getTranslation(){
        return translation;
    }

    public void clearBundleCache() {
        bundler.clearCache();
    }
    
}
