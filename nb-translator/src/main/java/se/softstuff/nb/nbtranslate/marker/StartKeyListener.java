/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.softstuff.nb.nbtranslate.marker;

import java.awt.KeyEventDispatcher;
import java.awt.event.KeyEvent;
import javax.swing.KeyStroke;

/**
 *
 * @author Stefan
 */
public class StartKeyListener implements KeyEventDispatcher{

    private final KeyStroke startKey;

    public StartKeyListener(KeyStroke startKey) {
        this.startKey = startKey;
    }
    
    
    @Override
    public boolean dispatchKeyEvent(KeyEvent e) {
         KeyStroke pressed = KeyStroke.getKeyStrokeForEvent(e);
        if (pressed.getKeyEventType() == KeyEvent.KEY_RELEASED && pressed.getModifiers() == startKey.getModifiers() && pressed.getKeyCode() == startKey.getKeyCode()) {
            TracerManager.getInstance().startTrackerAndMark(e.getComponent());
            return true;
        }
        return false;
    }
    
}
