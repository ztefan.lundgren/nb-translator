/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.softstuff.nb.nbtranslate.data;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataNode;

//Expression DATAOBJECT_REGISTRATION_IMPORT is undefined on line 11, column 3 in Templates/NetBeansModuleDevelopment-files/templateDataObjectInLayer.java.
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.nodes.CookieSet;
import org.openide.nodes.Node;
import org.openide.nodes.Children;
import org.openide.util.Lookup;
import org.openide.text.DataEditorSupport;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import se.softstuff.nb.nbtranslate.marker.TracerManager;


public class NbTranslationDataObject extends MultiDataObject {

    public static final String MIME_TYPE = "text/nb-bundle-diff+xml";
    private final Logger log = Logger.getLogger(getClass().getName());
    private NbTranslation content;
    
    public NbTranslationDataObject(FileObject pf, MultiFileLoader loader) throws DataObjectExistsException, IOException {
        super(pf, loader);
        CookieSet cookies = getCookieSet();
        cookies.add(new NbtOpenCookie(this));
        cookies.add((Node.Cookie) DataEditorSupport.create(this, getPrimaryEntry(), cookies));
        
        
    }

    @Override
    protected Node createNodeDelegate() {
        return new DataNode(this, Children.LEAF, getLookup());
    }

    @Override
    public Lookup getLookup() {
        return getCookieSet().getLookup();
    }
    

    public NbTranslation getContent() {
        if(content == null){

//                lock = takePrimaryFileLock();
                content = read();
                content.setFilename(getName());
                content.addPropertyChangeListener(new ChangeTracker());
            CookieSet cookies = getCookieSet();

                cookies.add(new Saver());

        }
        
        return content;
    }



    private NbTranslation read() {

        log.finest("getContent begin");
        InputStream inputStream = null;
        InputStreamReader reader = null;
        try {
            inputStream = getPrimaryFile().getInputStream();
            reader = new InputStreamReader(inputStream, XmlHandler.UTF8);
            NbTranslation sot = XmlHandler.fromXml(reader);
            log.finest("getContent end");
            return sot;
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        } catch (Exception ex) {
            log.log(Level.SEVERE,"failde read sot", ex);
            throw new RuntimeException(ex);
        } finally {
            try {
                inputStream.close();
            } catch (IOException ignore) {
            }
            try {
                reader.close();
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }

//    private void tracer(boolean on) {
//        if (on) {
//            getCookieSet().assign(StartTracerCookie.class);
//            getCookieSet().assign(StopTracerCookie.class, stopTracerSupport);
//        } else {
//            getCookieSet().assign(StartTracerCookie.class, startTrackerSupport);
//            getCookieSet().assign(StopTracerCookie.class);
//
//        }
//    }

    private TracerManager getTracer() {
        return Lookup.getDefault().lookup(TracerManager.class);
    }

    private static final Icon ICON = ImageUtilities.loadImageIcon("se/softstuff/nb/domain/save.png", true);

    
 
//    private class MySavable extends AbstractSavable implements Icon {
//        MySavable() {
//            register();
//        }
// 
//        @Override
//        protected String findDisplayName() {
//            return getName();
//        }
//
//        @Override
//        protected void handleSave() throws IOException {
//            
//            doSave();
//        }
// 
//
//        @Override
//        public boolean equals(Object obj) {
//            if (obj instanceof MySavable) {
//                MySavable m = (MySavable)obj;
//                return findDisplayName().equals(m.findDisplayName());
//            }
//            return false;
//        }
//
//        @Override
//        public int hashCode() {
//            return NbBundleDiffDataObject.this.hashCode();
//        }
//
//        @Override
//        public void paintIcon(Component c, Graphics g, int x, int y) {
//            ICON.paintIcon(c, g, x, y);
//        }
//
//        @Override
//        public int getIconWidth() {
//            return ICON.getIconWidth();
//        }
//
//        @Override
//        public int getIconHeight() {
//            return ICON.getIconHeight();
//        }
//        
//        public void doSave() throws IOException {
//            Writer writer = null;
//                      
//            
//            Entry primaryEntry = getPrimaryEntry();
//            boolean locked = primaryEntry.isLocked();
//            FileLock lock = takePrimaryFileLock();
//            FileObject file = primaryEntry.getFile();
//            
//            try {
//                OutputStream output = file.getOutputStream(lock);
//                writer = new OutputStreamWriter(output, NbBundleDiffXmlHandler.UTF8);
//                NbBundleDiffXmlHandler.toXml(getContent(), output);
//                setModified(false);
//
//            } finally {
//                lock.releaseLock();
//                try {
//                    if (writer != null) {
//                        writer.close();
//                    }
//                } catch (IOException ignore) {
//                }
//            }
//
//        }
// 
//    } 
    
    private class Saver implements SaveCookie {

        @Override
        public void save() throws IOException {
            Writer writer = null;
                      
            
            Entry primaryEntry = getPrimaryEntry();
            boolean locked = primaryEntry.isLocked();
            FileLock lock = takePrimaryFileLock();
            FileObject file = primaryEntry.getFile();
            
            try {
                OutputStream output = file.getOutputStream(lock);
                writer = new OutputStreamWriter(output, XmlHandler.UTF8);
                XmlHandler.toXml(getContent(), output);
                setModified(false);

            } finally {
                lock.releaseLock();
                try {
                    if (writer != null) {
                        writer.close();
                    }
                } catch (IOException ignore) {
                }
            }

        }
    }

    private class ChangeTracker implements PropertyChangeListener {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            log.fine("setModified(TRUE)  ChangeTracker.propertyChange: "+evt+" ");
            NbTranslationDataObject.this.setModified(true);
//            if (getLookup().lookup(MySavable.class) == null) {
//                ic.add(new MySavable());
//            }
        }
    }
//
//    private class StartTracerSupport implements StartTracerCookie {
//
//        @Override
//        public void startAndMark(Component comp) {
//            tracer(true);
//            getTracer().startTrackerAndMark(comp);
//        }
//    }
//
//    private class StopTracerSupport implements StopTracerCookie {
//
//        @Override
//        public void stop() {
//            tracer(false);
//            getTracer().stopTracker();
//        }
//    }
}
