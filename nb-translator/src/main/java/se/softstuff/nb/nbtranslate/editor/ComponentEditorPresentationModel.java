package se.softstuff.nb.nbtranslate.editor;

import java.awt.Button;
import java.awt.Checkbox;
import java.awt.Component;
import java.awt.Container;
import java.awt.TextArea;
import java.awt.TextComponent;
import java.awt.TextField;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javax.swing.AbstractButton;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.text.JTextComponent;
import org.openide.util.editable.BrandedLocale;
import org.openide.util.editable.BrandedLocaleBuilder;
import org.openide.util.editable.EditableResourceBundle;
import org.openide.util.editable.NbBundle;
import se.softstuff.nb.nbtranslate.data.NbTranslation;
import se.softstuff.nb.nbtranslate.marker.MarkerUtilities;
import se.softstuff.nb.nbtranslate.marker.MarkerUtilities.BundlePath;

/**
 *
 * @author stefan
 */
public class ComponentEditorPresentationModel {

    private Component component;
    private List<EditableResourceBundle> bundles;
    private NbTranslation translation;
    private EditableBundler editableBundler;
    
    
    void init(Component componentToTranslate, NbTranslation translation, EditableBundler editableBundler) {
        component = componentToTranslate;
        this.editableBundler = editableBundler;        
        this.translation = translation;
        
        findBundlesInParentPath(componentToTranslate);
        findBundlesInChildPath(componentToTranslate);
    }

    private void findBundlesInParentPath(Component forComponent) {
        
        editableBundler.clearCache();
        bundles = new ArrayList<EditableResourceBundle>();
                
        while(forComponent != null){
            addBundlesForClass(forComponent);
            
            forComponent = forComponent.getParent();
        }
    }
    
    private void findBundlesInChildPath(Component forComponent) {
        
        if(forComponent instanceof Container){
            Container container = (Container)forComponent;
            for(Component child : container.getComponents()){
                addBundlesForClass(child);
                findBundlesInChildPath(child);
            }
        }
    }
    
    private void addBundlesForClass(Component component) {
        Class clazz = component.getClass();
        String resourceBase = MarkerUtilities.findResoursePath(clazz)+"Bundle";
        resourceBase = resourceBase.replaceAll("/", ".");
        
        ClassLoader classLoader = clazz.getClassLoader();
        BundlePath bundlePaths = MarkerUtilities.findAllBundlePaths(clazz);
        if(bundlePaths != null){
            for(String resource : bundlePaths.bundleResoursePaths){
                BrandedLocale locale = BrandedLocaleBuilder.parse(resource);
                EditableResourceBundle bundle = editableBundler.getBundle(locale.getBranding(), resourceBase, locale.getLocale(), classLoader, false);
                bundles.add(bundle);
            }
        }
    }
    

    String findComponentText(Component comp) {
        String text = null;
        
        if(comp instanceof TextComponent){ // TextArea, TextField
            text = ((TextComponent)comp).getText();            
        } else if(comp instanceof JTextComponent){ // JEditorPane, JTextArea, JTextField
            text = ((JTextComponent)comp).getText();            
        } else if(comp instanceof Button){
            text = ((Button)comp).getLabel();
        }else if(comp instanceof Checkbox){
            text = ((Checkbox)comp).getLabel();
        } else if(comp instanceof AbstractButton){ // JButton, JMenuItem, JToggleButton, JCheckBox, JRadioButton
            text = ((AbstractButton)comp).getText();
        } else if(comp instanceof JLabel){
            text = ((JLabel)comp).getText();
        } 
        return text;
    }
    
    void setComponentText( String newText) {
        setComponentText(component, newText);
    }
            
    static void setComponentText(Component comp, String newText) {
        
        if(comp instanceof TextArea){ 
            ((TextArea)comp).setText(newText);    
        } else if(comp instanceof TextField){ 
            ((TextField)comp).setText(newText); 
        } else if(comp instanceof JTextComponent){ // JEditorPane, JTextArea, JTextField
            ((JTextComponent)comp).setText(newText);            
        } else if(comp instanceof Button){
            ((Button)comp).setLabel(newText);
        }else if(comp instanceof Checkbox){
            ((Checkbox)comp).setLabel(newText);
        } else if(comp instanceof AbstractButton){ // JButton, JMenuItem, JToggleButton, JCheckBox, JRadioButton
            ((AbstractButton)comp).setText(newText);
        } else if(comp instanceof JLabel){
            ((JLabel)comp).setText(newText);
        }
    }

    List<SearchElementResultRow> search(String nameSearch, String textSearch) {
        List<SearchElementResultRow> rows = new ArrayList<SearchElementResultRow>();
        
        for(EditableResourceBundle bundle : bundles) {
            Enumeration<String> keys = bundle.getKeys();
            String binar = bundle.getBundleFile();
            String resource = createResourceKey(bundle.getResourceBaseName(),bundle.getBrandingToken(),bundle.getLocale());
            if(!isLocalePartOfCurrent(bundle.getLocale())){
                continue;
            }
            String language = bundle.getLocale().toString();
            String module = MarkerUtilities.locateModuleRef(binar);
            
            while(keys.hasMoreElements()){
                String key = keys.nextElement();
                String value = bundle.getString(key);
                String searchable = value.replaceAll("&", "");
                String original = bundle.getOrginalString(key);
                
                if(containsOrNull(key,nameSearch) && (containsOrNull(searchable,textSearch) || containsOrNull(original,textSearch))){                   
                    
                    SearchElementResultRow row = new SearchElementResultRow(key, original, value, language, module, resource);
                    rows.add(row);
                }
            }
        }
        
        return rows;
    }
   private boolean containsOrNull(String value, String compare){
       if(compare == null || value == null || compare.isEmpty() || value.isEmpty()){
           return true;
       }
       return value.toLowerCase().contains(compare.toLowerCase());
   }

    void editRow(SearchElementResultRow row, String newValue) {
        translation.addResourceChange(row.getModule(), row.getResource(), row.getKey(), newValue);
        row.setValue(newValue);
    }
    
    void editLanguageRow(LanguageRowModel row, String newValue) {
        translation.addResourceChange(row.getModule(), row.getResource(), row.getKey(), newValue);
        row.setNewValue(newValue);
    }

    String createResourceKey(String resourceBaseName, String brandingToken, Locale locale) {
        StringBuilder sb = new StringBuilder(resourceBaseName);
        if(brandingToken != null){
            sb.append("_").append(brandingToken);
        }
        if(locale != null && !locale.toString().isEmpty()){
            sb.append("_").append(locale);
        }
        sb.append(".properties");
        return sb.toString();
    }

    private Collection<LanguageRowModel> createLanguageRows(BundlePath bundlePaths, SearchElementResultRow selected) {
        List<LanguageRowModel> rows = new LinkedList<LanguageRowModel>();
        String module = getModule(bundlePaths.jarName);
        
        if(!module.equals(selected.getModule())){
            return rows;
        }
        List<String> resources = new ArrayList<String>(bundlePaths.bundleResoursePaths);
        List<String> languages = new ArrayList<String>(bundlePaths.bundleTypes);
        if(resources.size() != languages.size()){
            throw new IllegalArgumentException(String.format("resources.size %s != languages.size %s for ", resources.size(), languages.size(), bundlePaths));
        }
        
        BrandedLocale bl = new BrandedLocale(NbBundle.getBranding(), Locale.getDefault() );
        String currentBrandingAndLocale = bl.getFilePart();
        for(int i=0; i< resources.size(); i++) {
            String resource = resources.get(i).replace("/", ".");
//            if(!resource.equals(selected.getResource())){
//                continue;
//            }
            
            for(EditableResourceBundle bundle : bundles){
                String currentFile = bundle.getBundleFile();
                if(currentFile == null) {
                    continue;
                }
                
                String currentResourceFile = MarkerUtilities.locateResourceFile(currentFile);
                
                String currentResourcePrefix = MarkerUtilities.locateResourcePath(currentFile);
                currentResourcePrefix = currentResourcePrefix.replaceAll("/", ".")+"Bundle";
                
                String currentModule = MarkerUtilities.locateModuleRef(currentFile);
                String language = MarkerUtilities.getBundleBrandingAndLocaleString(currentFile);
                String languageFilePart = (language.isEmpty()? language : "_"+language);
                if(languageFilePart.equals(currentBrandingAndLocale)){
                    currentBrandingAndLocale = null;
                }
                if(currentModule.equals(module) && selected.getResource().startsWith(currentResourcePrefix)) {
                    String orgValue = bundle.getOrginalString(selected.getKey());
                    
                    String newValue = orgValue;
                    if(bundle.containsKey(selected.getKey())){
                        newValue = bundle.getString(selected.getKey());
                    }
                    LanguageRowModel row = new LanguageRowModel(currentModule, currentResourceFile, language, selected.getKey(), orgValue, newValue);
                    rows.add(row);
                }
            } 
        }
        
        // add one empty row in the botton for the current running language
        if(currentBrandingAndLocale!=null && !rows.isEmpty()){
            String resource = rows.get(0).getResource();
            int end = resource.indexOf("Bundle");
            String newResourceFile = String.format("%sBundle%s.properties", resource.substring(0,end).replaceAll("/", "."), currentBrandingAndLocale);
            LanguageRowModel row = new LanguageRowModel(module, newResourceFile, currentBrandingAndLocale, selected.getKey(), "", "");
            rows.add(row);
        }
        
        return rows;
    }

    
    Collection<LanguageRowModel> findLanguageRowsFor(SearchElementResultRow selected) {
        Set<LanguageRowModel> result = new HashSet<LanguageRowModel>();
        Component current = component;
        while(current != null){
            
            BundlePath bundlePaths = MarkerUtilities.findAllBundlePaths(current.getClass());
            
            Collection<LanguageRowModel> rows = createLanguageRows(bundlePaths, selected); 
            result.addAll(rows);
            
            current = current.getParent();
        }
        
        return result;
    }

    private String getModule(String jarName) {
        int end = jarName.lastIndexOf(".");
        if(end != -1){
            jarName = jarName.substring(0, end);
        }
        return jarName;
    }

    private boolean isLocalePartOfCurrent(Locale locale) {

        Locale current = Locale.getDefault();
        if(locale.getLanguage().equals(current.getLanguage())){
            return true;
        }
        if(locale.getCountry().equals(current.getCountry())){
            return true;
        }
        return locale.equals(current);
    }

    

    
    
}
