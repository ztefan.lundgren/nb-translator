/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.softstuff.nb.nbtranslate.manager;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.cookies.OpenCookie;
import org.openide.loaders.DataObject;
import org.openide.windows.WindowManager;
import se.softstuff.nb.nbtranslate.data.NbTranslationDataObject;

public final class OpenAction implements ActionListener {

    private final DataObject context;

    public OpenAction(DataObject context) {
        this.context = context;
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        
        if(context instanceof NbTranslationDataObject){
            OpenCookie cookie = context.getCookie(OpenCookie.class);
            cookie.open();
        }
    }
}
