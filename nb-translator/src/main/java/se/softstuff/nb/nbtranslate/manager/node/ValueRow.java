/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.softstuff.nb.nbtranslate.manager.node;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

/**
 *
 * @author Stefan
 */
public class ValueRow {
    
    public static final String PROP_CHANGED = "changed";
    
    private String module;
    private String resouce;
    private String key;
    private String origin;

    private PropertyChangeSupport pcs;
    
    public ValueRow(String module, String resouce, String key, String origin, String changed) {
        this.module = module;
        this.resouce = resouce;
        this.key = key;
        this.origin = origin;
        this.changed = changed;
        
        pcs = new PropertyChangeSupport(this);
    }
        private String changed;


    public String getChanged() {
        return changed;
    }

    public void setChanged(String changed) {
        String oldChanged = this.changed;
        this.changed = changed;
        pcs.firePropertyChange(PROP_CHANGED, oldChanged, changed);
    }


    public String getModule() {
        return module;
    }

    public String getResouce() {
        return resouce;
    }

    public String getKey() {
        return key;
    }

    public String getOrigin() {
        return origin;
    }
    
    
    public void addPropertyChangeListener(PropertyChangeListener listener){
        pcs.addPropertyChangeListener(listener);
    }
}
