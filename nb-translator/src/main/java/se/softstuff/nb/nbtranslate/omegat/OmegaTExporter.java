package se.softstuff.nb.nbtranslate.omegat;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import se.softstuff.nb.nbtranslate.data.NbTranslation;
import static se.softstuff.nb.nbtranslate.util.EncodingUtil.*;
import static se.softstuff.nb.nbtranslate.omegat.DirectoryUtil.*;

/**
 *
 * @author mats
 */
final class OmegaTExporter {

    private final NbTranslation translation;

    private final FileObject projectDir;

    private final boolean removeOld;

    private String relevantLocale;
    
    static int export(NbTranslation translation, FileObject projectDir, boolean removeOld, String selectedLocale) throws IOException {
        final OmegaTExporter exporter = new OmegaTExporter(translation, projectDir, removeOld);
        exporter.setRelevantLocale(selectedLocale);
        return exporter.export();
    }

    public OmegaTExporter(NbTranslation translation, FileObject projectDir, boolean removeOld) {
        this.translation = translation;
        this.projectDir = projectDir;
        this.removeOld = removeOld;
    }

    int export() throws IOException {
        int rows = 0;
        FileLock lock = projectDir.lock();
        try {
            FileObject source = getSourceFolder();
            for (String module : translation.getOriginalModulesNames()) {
                FileObject moduleDir = FileUtil.createFolder(source, module);
                Map<String, Properties> resources = translation.getOriginalResource(module);
                rows += exportResource(resources, moduleDir);
            }
        } finally {
            lock.releaseLock();
        }
        return rows;
    }

    private FileObject getSourceFolder() throws IOException {
        FileObject source = getChildDir(projectDir, "source");
        if (source != null && removeOld) {
            source.delete();
        }
        source = FileUtil.createFolder(projectDir, "source");
        return source;
    }

    private int exportResource(Map<String, Properties> resources, FileObject moduleDir) throws IOException {
        int rows = 0;
        for (String resource : resources.keySet()) {
            if (!resourceIsRelevant(resource)) {
                continue;
            }
            String exportFilename = createDefaultBundleFile(resource);
            Properties prop = resources.get(resource);
            FileObject resourceFile = FileUtil.createData(moduleDir, exportFilename);
            PrintStream out = new PrintStream(resourceFile.getOutputStream(), true, ENCODING);
            list(prop, out);
            out.close();
            rows += prop.size();
        }
        return rows;
    }

    private void list(Properties prop, PrintStream out) {

        boolean escUnicode = true;
        for (Enumeration e = prop.keys(); e.hasMoreElements();) {
            String key = (String) e.nextElement();
            String val = (String) prop.get(key);
            key = saveConvert(key, true, escUnicode);
            /* No need to escape embedded and trailing spaces for value, hence
             * pass false to flag.
             */
            val = saveConvert(val, false, escUnicode);
            out.print(key);
            out.print("=");
            out.println(val);
        }
    }

    /**
     * Only visible for testability.
     */
    boolean resourceIsRelevant(String resource) {
        if (relevantLocale == null) {
            return resource.indexOf("_") == -1;
        }
        String propertiesName = relevantLocale + ".properties";
        return resource.endsWith(propertiesName);
    }

    void setRelevantLocale(String relevantLocale) {
        this.relevantLocale = relevantLocale;
    }

    private String createDefaultBundleFile(String resource) {
        int end = resource.indexOf(".Bundle");
        String exportFilename = String.format("%s.Bundle.properties", resource.substring(0, end));
        return exportFilename;
    }
}
