package se.softstuff.nb.nbtranslate.swing;

import java.io.File;
import java.io.IOException;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author stefan
 */
public class NbBundleDiffFileFilter extends FileFilter {

    @Override
    public boolean accept(File f) {
        try {
            return f.isDirectory() || f.isFile() && f.getCanonicalPath().endsWith(".nbt");
        } catch (IOException ex) {
            return false;
        }
    }

    @Override
    public String getDescription() {
        return "*.nbt";
    }
}
